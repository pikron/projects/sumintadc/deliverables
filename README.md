# Implementation of Sum-Int ADC IP-core into radiation-tolerant FPGA and performance evaluation
## GSTP Element 1,  De-risk Project

|Label |Value |
| ------ | ------ |
|ESA Contract No.:   |4000134870/21/NL/GLC/rk |
|ESA Star reference: |1000030143-8000007722-1 |
|Company:            |PiKRON, s.r.o. (Czech Republic) |
|ESA Entity Code:    |1 000 022 945 |
|Lead Author:        |Pavel Pisa (ppisa@pikron.com) |

Deliverables in documentation category:

* DHW1 – Preliminary Analog Front-end Schematics and Board Layout
  * Schematic Diagram of SumIntADC_v3 board Rev 1.0 [PDF](DHW1/fabrication-outputs/SumIntADC_v3.pdf)
  * [folder with all files](DHW1)
* DTP1 – Test Plan [PDF](DTP1/SUMINTADC-PIKRON-DTP1-rev2.pdf) [ODT](DTP1/SUMINTADC-PIKRON-DTP1-rev2.odt)
* DCR1 – Characterization Report [PDF](DCR1/SumIntADC-PIKRONDCR1-CTU-REPORT-REV3.pdf) [DOCX](DCR1/SumIntADC-PIKRONDCR1-CTU-REPORT-REV3.docx)
  * Annex 1: SumInt ADC Measurements Diary [PDF](DCR1/measurements-diary.pdf) [ODS](DCR1/measurements-diary.ods)
  * Annex 2: SumInt ADC Absolute Errors Evaluation [PDF](DCR1/sumintadc-char-totals.pdf) [ODS](DCR1/sumintadc-char-totals.ods)
* DHW2 – Final Analog Front-end Schematics and Board Layout
  * Schematic Diagram of SumIntADC_v3 board Rev 1.0 [PDF](DHW2/fabrication-outputs/SumIntADC_v3.pdf)
  * Schematic Diagram of SumIntADC_v3 board Rev 1.2 [PDF](DHW2/fabrication-outputs/SumIntADC_v3_rev1_2.pdf) (final components values and minor corrections)
  * Components Placement for SumIntADC_v3 board Rev 1.0 [PDF](DHW2/fabrication-outputs/gerber/SumIntADC_v3-F_Fab.pdf)
  * [fabrication outputs](DHW2/fabrication-outputs)
  * Test support digital insulator board [isolator_4_plus_4](DHW2/isolator_4_plus_4) data
  * [folder with all files](DHW2)
  * [GIT Used for Project Development](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/tree/master/work/test-boards/SumIntADC_NX_breadboard)
* DDD1 – Design Description and Implementation Guide [PDF](DDD1/SumIntADC-PIKRON-DDD1-Design_Guide.pdf) [ODT](DDD1/SumIntADC-PIKRON-DDD1-Design_Guide.odt)
  * Annex Theory: Σ-Integration Analog to Digital Converter, Idea, Implementation and Results - Extended [PDF](DDD1/SumIntADC-PIKRON-DDD1-Annex-Theory.pdf)
  * Power supplies stability analysis and tuning [PDF](DDD1/supplyFRA.pdf) [ODT](DDD1/supplyFRA.odt)
* DUG1 – Demo Kit User Documentation/Quick Start Guide [PDF](DUG1/SumIntADC-PIKRON-DUG1.pdf) [ODT](DUG1/SumIntADC-PIKRON-DUG1.odt)
* FP – Final Presentation [PDF](FP/SumIntADC-PIKRON-FP.pdf) [ODP](FP/SumIntADC-PIKRON-FP.odp)
* FR – Final Report [PDF](FR/SumIntADC-PIKRON-FR-Final_Report.pdf) [ODT](FR/SumIntADC-PIKRON-FR-Final_Report.odt)
* TAS – Technology Achievement Summary [PDF](TAS/GSTP-4000134870-SumIntADC-Technology_Achievement.pdf) [PPTX](TAS/GSTP-4000134870-SumIntADC-Technology_Achievement.pptx)
* ESR – Executive Summary Report [PDF](ESR/SumIntADC-PIKRON-ESR.pdf) [ODT](ESR/SumIntADC-PIKRON-ESR.odt)

Other deliverables
* HDL1 – Preliminary HDL Design [folder](HDL1)
* HDL2 – Final HDL Design [folder](HDL2)
  * the Sum-Int ADC core is implemented by [HDL2/sumint_nx_bb_v3/src/siroladc.vhdl](HDL2/sumint_nx_bb_v3/src/siroladc.vhdl) component
  * Two Wire Serial Interface (TWSI - simple unidirectional link with Space-Wire like bits encoding method) is implemented in nanoXplore design top level [sumint_nx_bb_v3/src](sumint_nx_bb_v3/src)
  * TWSI data reception on Xilinx Zynq side is implemented in Vivado component [HDL2/twsi_capt_1.0](HDL2/twsi_capt_1.0)
  * [development git](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/tree/master/work/test-hdl/sumint_nx_bb_v3)
* HW1 – Final Analog Front-end Breadboard
  * 1 piece delivered to ESA, 3 deposited at PiKRON s.r.o.
  * [Wiki Page](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/wikis/sumint-breadboard-v1)
  * [development git](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/tree/master/work/test-boards/SumIntADC_NX_breadboard)
* SW1 – Final Test Software for Linux Host System for Sum-Int ADC Control and Data Acquisition
  * [SW1 deliverable folder](SW1)
  * [development git](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/tree/master/work/test-sw)
  * [GPIB metrological lab automation control software](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/tree/master/work/test-sw/sumintadc-char-automation)
  * [acquired characterization data processing software and scripts](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/tree/master/work/data-processing)

Delivered separately
* CCD – Contract Closure Documentation
* DP – Development Plan for a potential follow-on activity
* PH – Photographic Documentation
* TDP – Technical Data Package
  * all characterization data already delivered

Project Resources
* Main project site: [https://gitlab.com/pikron/projects/sumintadc](https://gitlab.com/pikron/projects/sumintadc)
* Project development Wiki: [https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/wikis/home](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/wikis/home)
* PiKRON s.r.o. company pages: [https://www.pikron.com/](https://www.pikron.com/)

Project related articles

* PÍŠA, Pavel; PORAZIL, Petr: Σ-Integration Analog to Digital Converter, Idea, Implementation and Results. IFAC Proceedings Volumes, 2005, 38.1: 85-90., Congress of the International Federation of Automatic Control. 16th IFAC World Congress:  article online https://www.sciencedirect.com/science/article/pii/S1474667016372056

* PÍŠA, Pavel; PORAZIL, Petr; LADMAN, Jakub; LEVAC, David; PECA, Marek; SEDLÁČEK, Radek; ŠPAČEK, Michal: [Implementation and Evaluation of Sum-Int ADC IP-core on NanoXplore FPGA](https://indico.esa.int/event/388/contributions/6724), 2022, [9th International Workshop on Analogue and Mixed-Signal Integrated Circuits for Space Applications](https://indico.esa.int/event/388), [article PDF](https://indico.esa.int/event/388/contributions/6724/attachments/4569/7000/AMICSA_SumIntADC.pdf), [slides PDF](https://indico.esa.int/event/388/contributions/6724/attachments/4569/6977/AMICSA_SUMINTADC-Slides.pdf)
