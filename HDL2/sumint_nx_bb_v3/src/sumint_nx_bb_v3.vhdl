library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

--use work.util.all;

-- NXmap v2.9.5 (NXmap3 2020.1 had a LVDS out bug)

library NX;
use NX.nxPackage.all;

entity sumint_nx_bb_v3 is
  generic (
    n_adc: natural := 6;
    n_adc_refout: natural := 2;
    adc_width: natural := 16;
    adc_cycle: natural := 500; -- 125
    --
    n_hr_adc: natural := 2;
    hr_adc_width: natural := 16;
    hr_adc_cycle: natural := 10000; -- 125
    --
    n_twsi_tx_data_bytes : natural := 6 * 2 + 2 * 2 + 4;
    n_twsi_tx_element_len : natural := 16;
    n_twsi_freq_div : natural := 2
    );
  port (
    osc0 : in std_logic;
    --
    siadc_ain : in std_logic_vector (n_adc - 1 downto 0);
    siadc_refsw : out std_logic_vector (n_adc - 1 downto 0);
    siadc_refout : out std_logic_vector (n_adc_refout - 1 downto 0);
    --
    siadc_hr_ain : in std_logic_vector (n_hr_adc - 1 downto 0);
    siadc_hr_refsw_n : out std_logic_vector (n_hr_adc - 1 downto 0);
    --
    twsi_do : out std_logic;
    twsi_so : out std_logic;
    --
    btn  : in std_logic_vector (1 downto 0);
    led  : out std_logic_vector (7 downto 0)
    );
end sumint_nx_bb_v3;

architecture rtl of sumint_nx_bb_v3 is
  component siroladc is
    generic (
      width: natural
    );
    port (
      -- reset: in std_logic;
      clock: in std_logic;
      sync: in std_logic;
      pcounter_value: out std_logic_vector (width-1 downto 0);
      adc_mux: out std_logic;
      adc_cmp: in std_logic
    );
  end component;

  signal clock, clock_x2, clock_x4, pll_lock, reset_sync: std_logic;
  signal rst_reg: std_logic_vector (2 downto 0);

  type adc_matrix_t is array (0 to n_adc-1) of
    std_logic_vector (adc_width-1 downto 0);
  signal adc_word: adc_matrix_t;
  signal last_adc_word: adc_matrix_t;
  signal prev_adc_word: adc_matrix_t;
  signal adc_mon_word: adc_matrix_t;
  signal next_adc_mon_word: adc_matrix_t;
  signal clock_adc_mod: std_logic;
  signal adc_cycle_cnt: integer range 0 to adc_cycle-1;
  signal adc_sync, next_adc_sync: std_logic;
  signal sync2fast, last_sync2fast, next_sync2fast: std_logic;

  type hr_adc_matrix_t is array (0 to n_hr_adc-1) of
    std_logic_vector (hr_adc_width-1 downto 0);
  signal hr_adc_word: hr_adc_matrix_t;
  signal hr_last_adc_word: hr_adc_matrix_t;
  signal hr_adc_cycle_cnt: integer range 0 to hr_adc_cycle-1;
  signal hr_adc_sync, hr_next_adc_sync: std_logic;
  signal hr_sync2fast, hr_last_sync2fast, hr_next_sync2fast: std_logic;
  signal siadc_hr_refsw : std_logic_vector (n_hr_adc - 1 downto 0);

  constant twsi_tx_data_header : std_logic_vector (7 downto 0) := "11110101";
  constant twsi_tx_data_msgid_bits : natural := 8;
  constant twsi_tx_data_len_bits : natural := 8;
  constant twsi_tx_data_seq_bits : natural := 8;
  constant twsi_tx_data_msgid_idx : natural := twsi_tx_data_header'length;
  constant twsi_tx_data_seq_idx : natural := twsi_tx_data_msgid_idx + twsi_tx_data_msgid_bits;
  constant twsi_tx_data_len_idx : natural := twsi_tx_data_seq_idx + twsi_tx_data_msgid_bits;
  constant twsi_tx_data_payload_idx : natural := twsi_tx_data_len_idx + twsi_tx_data_seq_bits;

  constant twsi_tx_data_msgid_adc : std_logic_vector (twsi_tx_data_msgid_bits-1 downto 0) := x"10";
  constant twsi_tx_data_adc_idx : natural := twsi_tx_data_payload_idx;
  constant twsi_tx_data_hr_adc_idx : natural := twsi_tx_data_adc_idx + n_twsi_tx_element_len * n_adc;
  constant twsi_tx_data_padding_idx : natural := twsi_tx_data_hr_adc_idx + n_twsi_tx_element_len * n_hr_adc;
  constant twsi_tx_data_frame_bits : natural := n_twsi_tx_data_bytes * 8 + twsi_tx_data_payload_idx;
  signal twsi_tx_rq : std_logic;
  signal twsi_tx_busy : std_logic;
  signal twsi_tx_data: std_logic_vector (twsi_tx_data_frame_bits - 1 downto 0);
  signal twsi_tx_shr: std_logic_vector (twsi_tx_data_frame_bits - 1 downto 0);
  signal twsi_tx_bit_cnt: integer range 0 to twsi_tx_data_frame_bits - 1;
  signal twsi_tx_freq_div_cnt: integer range 0 to n_twsi_freq_div - 1;
  signal twsi_do_st : std_logic;
  signal twsi_so_st : std_logic;
  signal twsi_tx_data_seq : std_logic_vector (twsi_tx_data_seq_bits - 1 downto 0);

begin
  --
  -- osc0=25MHz, f_vco=400MHz, clock = 100MHz, clock_x2 = 200MHz, clock_x4 = 400MHz
  pll0: NX_PLL
    generic map (
                                                --    0: 200 -> 425 MHz
                                                --    1: 400 -> 850 MHz
      vco_range    => 1,                        --    2: 800 -> 1200 MHz
                                                -- No added %2 division
      ref_div_on   => '0',                      -- bypass :: %2
      fbk_div_on   => '0',                      -- bypass :: %2
      ext_fbk_on   => '0',
                                                -- if fbk_div_on = 0
                                                --   2 to 31 => %4 to %62
                                                -- else
      fbk_intdiv   => 8,                        --   1 to 15 => %4 to %60
                                                -- No Fbk Delay
      fbk_delay_on => '0',                      -- fbk delay not used ...
      fbk_delay    => 0,                        -- 0 to 63
      --clk_outdiv1  => 3, -- *16/8 =  50MHz      -- 0 to 7   (%1 to %2^7)
      clk_outdiv1  => 2, -- *16/4 = 100MHz      -- 0 to 7   (%1 to %2^7)
      clk_outdiv2  => 1, -- *16/1 = 200MHz      -- 0 to 7   (%1 to %2^7)
      clk_outdiv3  => 0  -- *16/2 = 400MHz      -- 0 to 7   (%1 to %2^7)
    )
    port map (
      REF  => osc0,
      FBK  => OPEN,
      VCO  => OPEN,
      D1   => clock,
      D2   => clock_x2,
      D3   => clock_x4,
      OSC  => OPEN,
      RDY  => pll_lock
      );


  clock_adc_mod <= clock_x2;

  reset_gen: process (pll_lock, rst_reg, clock)
  begin
    if pll_lock = '0' then
      rst_reg <= "111";
      reset_sync <= '1';
    elsif clock'event and clock = '1' then
      rst_reg <= rst_reg(1 downto 0) & (not pll_lock);
      reset_sync <= rst_reg(2) or rst_reg(1);
    end if;
  end process;

  siadc_refout <= (others=>'1');

  adc_block: for i in n_adc-1 downto 0 generate
    adc: siroladc
      generic map (width => adc_width)
      port map (
        clock => clock_adc_mod,
        sync => adc_sync,
        pcounter_value => adc_word(i)(adc_width-1 downto 0),
        adc_mux => siadc_refsw(i),
        adc_cmp => siadc_ain(i)
      );
  end generate;

  adc_monitor: for i in n_adc-1 downto 0 generate
    next_adc_mon_word(i) <= std_logic_vector(unsigned(last_adc_word(i)) -
                            unsigned(prev_adc_word(i)));
  end generate;

  next_adc_sync <= not last_sync2fast and sync2fast;

  adc_last: process(clock, adc_word, reset_sync)
  begin
    if reset_sync = '1' then
      adc_cycle_cnt <= 0;
      next_sync2fast <= '0';
      last_adc_word <= ((others=> (others=>'0')));
      prev_adc_word <= ((others=> (others=>'0')));
      adc_mon_word <= ((others=> (others=>'0')));
      twsi_tx_rq <= '0';
    elsif clock'event and clock = '1' then
      if adc_cycle_cnt = 0 then
        adc_cycle_cnt <= adc_cycle - 1;
        prev_adc_word <= last_adc_word;
        last_adc_word <= adc_word;
        adc_mon_word <= next_adc_mon_word;
        next_sync2fast <= '1';
        twsi_tx_rq <= '1';
      else
        adc_cycle_cnt <= adc_cycle_cnt - 1;
        next_sync2fast <= '0';
        twsi_tx_rq <= '0';
      end if;
    end if;
  end process;

  fast_domain_sync: process (clock_adc_mod, reset_sync, sync2fast, next_sync2fast, next_adc_sync)
  begin
    if reset_sync = '1' then
      sync2fast <= '0';
    elsif clock_adc_mod'event and clock_adc_mod = '1' then
      sync2fast <= next_sync2fast;
      last_sync2fast <= sync2fast;
      adc_sync <= next_adc_sync;
    end if;
  end process;

  led(7 downto 0) <= not adc_mon_word(0)(8 downto 1);

  --led(0) <= not di;
  --led(1) <= not si;
  --led(2) <= not bool_to_std(dbg_null);
  --led(3) <= not bool_to_std(rx_valid);
  --led(4) <= not bool_to_std(dbg_err);
  --led(6 downto 5) <= not rxd(1 downto 0);
  --led(7) <= not rx_clk;

  -- High Resolution ADC

  hr_next_adc_sync <= not hr_last_sync2fast and hr_sync2fast;

  hr_adc_block: for i in n_hr_adc-1 downto 0 generate
    adc: siroladc
      generic map (width => hr_adc_width)
      port map (
        clock => clock_adc_mod,
        sync => hr_adc_sync,
        pcounter_value => hr_adc_word(i)(hr_adc_width-1 downto 0),
        adc_mux => siadc_hr_refsw(i),
        adc_cmp => siadc_hr_ain(i)
      );
      siadc_hr_refsw_n(i) <= not siadc_hr_refsw(i);
  end generate;

  hr_adc_last: process(clock, hr_adc_word, reset_sync)
  begin
    if reset_sync = '1' then
      hr_adc_cycle_cnt <= 0;
      hr_next_sync2fast <= '0';
    elsif clock'event and clock = '1' then
      if hr_adc_cycle_cnt = 0 then
        hr_adc_cycle_cnt <= hr_adc_cycle - 1;
        hr_next_sync2fast <= '1';
        hr_last_adc_word <= hr_adc_word;
      else
        hr_adc_cycle_cnt <= hr_adc_cycle_cnt - 1;
        hr_next_sync2fast <= '0';
      end if;
    end if;
  end process;

  hr_fast_domain_sync: process (clock_adc_mod, reset_sync, hr_sync2fast, hr_next_sync2fast, hr_next_adc_sync)
  begin
    if reset_sync = '1' then
      hr_sync2fast <= '0';
    elsif clock_adc_mod'event and clock_adc_mod = '1' then
      hr_sync2fast <= hr_next_sync2fast;
      hr_last_sync2fast <= hr_sync2fast;
      hr_adc_sync <= hr_next_adc_sync;
    end if;
  end process;

  -- Two Wire Serial Interface (TWSI)

  -- Map ADC data into twsi_tx_data vector

  twsi_tx_data(twsi_tx_data_header'length-1 downto 0) <= twsi_tx_data_header;

  twsi_tx_data(twsi_tx_data_msgid_idx + twsi_tx_data_msgid_bits-1 downto twsi_tx_data_msgid_idx) <=  twsi_tx_data_msgid_adc;

  twsi_tx_data(twsi_tx_data_seq_idx + twsi_tx_data_seq_bits-1 downto twsi_tx_data_seq_idx) <=  twsi_tx_data_seq;

  twsi_tx_data(twsi_tx_data_len_idx + twsi_tx_data_len_bits-1 downto twsi_tx_data_len_idx) <=  std_logic_vector(to_unsigned(n_twsi_tx_data_bytes, twsi_tx_data_len_bits));

  twsi_tx_map_adc: for i in 0 to n_adc-1 generate
    constant idx0 : natural := i * n_twsi_tx_element_len + twsi_tx_data_adc_idx;
    constant data_bits : natural := min(adc_width, n_twsi_tx_element_len);
  begin
    twsi_tx_data(idx0 + n_twsi_tx_element_len-1 downto idx0) <=
                   (idx0 + data_bits-1 downto idx0 => adc_word(i), others => '0');
  end generate;

  twsi_tx_map_hr_adc: for i in 0 to n_hr_adc-1 generate
    constant idx0 : natural := i * n_twsi_tx_element_len + twsi_tx_data_hr_adc_idx;
    constant data_bits : natural := min(hr_adc_width, n_twsi_tx_element_len);
  begin
    twsi_tx_data(idx0 + n_twsi_tx_element_len-1 downto idx0) <=
                   (idx0 + data_bits-1 downto idx0 => hr_last_adc_word(i), others => '0');
  end generate;

  twsi_tx_data(twsi_tx_data_frame_bits-1 downto twsi_tx_data_padding_idx) <= (others => '0');

  -- Send data over two wire interface

  twsi_tx_process: process(clock, twsi_tx_rq, twsi_tx_data, reset_sync)
  begin
    if reset_sync = '1' then
      twsi_tx_bit_cnt <= 0;
      twsi_tx_freq_div_cnt <= 0;
      twsi_tx_busy <= '0';
      twsi_do_st <= '0';
      twsi_so_st <= '0';
      twsi_tx_data_seq <= (others => '0');
    elsif clock'event and clock = '1' then
      if twsi_tx_busy = '0' then
        twsi_do_st <= '0';
        twsi_so_st <= '0';
        twsi_tx_bit_cnt <= 0;
        if twsi_tx_rq = '1' then
          twsi_do_st <= twsi_tx_data(0);
          twsi_so_st <= not twsi_tx_data(0);
          twsi_tx_shr <= '0' & twsi_tx_data(twsi_tx_data_frame_bits - 1 downto 1);
          twsi_tx_busy <= '1';
          twsi_tx_freq_div_cnt <= n_twsi_freq_div - 1;
          twsi_tx_bit_cnt <= twsi_tx_data_frame_bits - 1;
        end if;
      elsif twsi_tx_freq_div_cnt /= 0 then
        twsi_tx_freq_div_cnt <= twsi_tx_freq_div_cnt - 1;
      elsif twsi_tx_bit_cnt /= 0 then
        twsi_tx_freq_div_cnt <= n_twsi_freq_div - 1;
        if twsi_do_st = twsi_tx_shr(0) then
          twsi_so_st <= not twsi_so_st;
        else
          twsi_so_st <= twsi_so_st;
        end if;
        twsi_do_st <= twsi_tx_shr(0);
        twsi_tx_shr <= '0' & twsi_tx_shr(twsi_tx_data_frame_bits - 1 downto 1);
        twsi_tx_bit_cnt <= twsi_tx_bit_cnt - 1;
      else
        twsi_do_st <= '0';
        twsi_so_st <= '0';
        twsi_tx_busy <= '0';
        twsi_tx_data_seq <= std_logic_vector(unsigned(twsi_tx_data_seq)+1);
      end if;
    end if;
  end process;

  twsi_do <= twsi_do_st;
  twsi_so <= twsi_so_st;

end architecture;
