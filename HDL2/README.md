# Implementation of Sum-Int ADC IP-core into radiation-tolerant FPGA and performance evaluation
# HDL2 – Final HDL Design

  * the Sum-Int ADC core is implemented by [sumint_nx_bb_v3/src/siroladc.vhdl](sumint_nx_bb_v3/src/siroladc.vhdl) component
  * Two Wire Serial Interface (TWSI - simple unidirectional link with Space-Wire like bits encoding method) is implemented in nanoXplore design top level [sumint_nx_bb_v3/src](sumint_nx_bb_v3/src)
  * TWSI data reception on Xilinx Zynq side is implemented in Vivado component [twsi_capt_1.0](twsi_capt_1.0)
