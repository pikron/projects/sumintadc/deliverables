--
-- Two Wire Serial Interface Channel receiver
-- Copyright (c) 2022 Pavel Pisa <ppisa@pikron.com>
--
-- API based on the Zlogan (Logic Analyzer) project by Marek Peca
-- Copyright (c) 2017 Marek Peca <mp@eltvor.cz>
--
-- This source file is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 2.1 of the License, or (at your option) any later version.
--
-- This source file is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
-- Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this library; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity twsi_receiver is
  generic (
    constant data_header : std_logic_vector (7 downto 0) := "11110101";
    rx_iddle_timeout: natural := 100;
    b_out: natural := 4 -- output FIFO width (4 or 8 [bytes])
  );
  port (
    reset: in std_logic;
    clock: in std_logic;
    en: in std_logic;
    out_first: out std_logic;
    out_valid: out std_logic;
    out_data: out std_logic_vector (b_out*8-1 downto 0);
    --
    rx_bit : out std_logic;
    rx_valid : out std_logic;
    --
    rx_di : in std_logic;
    rx_si : in std_logic
  );
end twsi_receiver;

architecture rtl of twsi_receiver is

  component twsi_rx_bit is
  port (
    clock : in std_logic;
    reset : in std_logic;
    --
    rx_bit : out std_logic;
    rx_valid : out std_logic;
    --
    rx_di : in std_logic;
    rx_si : in std_logic
    );
  end component;

  signal byte_shr: std_logic_vector (7 downto 0);
  signal byte_shr_next: std_logic_vector (7 downto 0);
  signal byte_shr_full_next: std_logic;
  signal data_buf: std_logic_vector (b_out*8-1 downto 0);
  signal data_buf_next: std_logic_vector (b_out*8-1 downto 0);
  signal data_buf_full_next: std_logic;
  signal byte_mask_shr : std_logic_vector (b_out-1 downto 0);
  signal byte_mask_shr_next : std_logic_vector (b_out-1 downto 0);
  signal bytes_cnt : integer range 0 to 255;
  signal data_phase : std_logic;
  signal head_received : std_logic;
  signal timeout_cnt : integer range 0 to rx_iddle_timeout;
  signal first_in_buf : std_logic;
  signal rx_bit_local : std_logic;
  signal rx_valid_local : std_logic;

begin

  twsi_receiver_rx_bit : twsi_rx_bit
  port map (
    reset => reset,
    clock => clock,
    --
    rx_bit => rx_bit_local,
    rx_valid => rx_valid_local,
    --
    rx_di => rx_di,
    rx_si => rx_si
  );

  rx_bit <= rx_bit_local;
  rx_valid <= rx_valid_local;

  byte_shr_next(6 downto 0) <= byte_shr(7 downto 1);
  byte_shr_next(7) <= rx_bit_local;
  byte_shr_full_next <= byte_shr(0);

  byte_mask_shr_next(b_out-1 downto 1) <= byte_mask_shr(b_out-2 downto 0);
  byte_mask_shr_next(0) <= byte_mask_shr(b_out-1);

  data_buf_next_process: process(data_buf, byte_shr_next, byte_mask_shr)
  begin
    for i in 0 to b_out-1 loop
      if byte_mask_shr(i) = '1' then
        data_buf_next(i * 8 + 7 downto i * 8) <= byte_shr_next;
      else
        data_buf_next(i * 8 + 7 downto i * 8) <= data_buf(i * 8 + 7 downto i * 8);
      end if;
    end loop;
    data_buf_full_next <= byte_mask_shr(b_out-1);
  end process;

  rx_data_process: process(clock)
  begin
    if rising_edge(clock) then
      out_valid <= '0';
      if reset = '1' then
        byte_shr <= (others => '0');
        data_buf <= (others => '0');
        byte_mask_shr <= ( 0 => '1', others => '0');
        bytes_cnt <= 0;
        data_phase <= '0';
        head_received <= '0';
        timeout_cnt <= rx_iddle_timeout;
        out_first <= '0';
        out_data <= ( others => '0');
        first_in_buf <= '1';
      elsif rx_valid_local = '1' then
        out_data <= data_buf_next;
        out_first <= first_in_buf;
        timeout_cnt <= rx_iddle_timeout;
        byte_shr <= byte_shr_next;
        if head_received = '0' then
          bytes_cnt <= 2;
          if byte_shr_next = data_header then
            head_received <= '1';
            data_buf <= data_buf_next;
            byte_mask_shr <= (1 => '1', others => '0');
            byte_shr <= (7 => '1', others => '0');
            first_in_buf <= '1';
            data_phase <= '0';
          else
            byte_mask_shr <= (0 => '1', others => '0');
          end if;
        elsif byte_shr_full_next = '1' then
          data_buf <= data_buf_next;
          byte_mask_shr <= byte_mask_shr_next;
          byte_shr <= (7 => '1', others => '0');
          if data_buf_full_next = '1' then
            out_valid <= '1';
            first_in_buf <= '0';
          end if;
          if bytes_cnt = 0 then
            if data_phase = '0' then
              bytes_cnt <= to_integer(unsigned(byte_shr_next)) - 1;
              if to_integer(unsigned(byte_shr_next)) /= 0 then
                data_phase <= '1';
              else
                data_phase <= '0';
                out_valid <= '1';
                head_received <= '0';
              end if;
            else
              data_phase <= '0';
              out_valid <= '1';
              head_received <= '0';
            end if;
          else
            bytes_cnt <= bytes_cnt - 1;
          end if;
        end if;
      else
        if timeout_cnt /= 0 then
          timeout_cnt <= timeout_cnt - 1;
        else
          data_phase <= '0';
          head_received <= '0';
          byte_shr <= (others => '0');
        end if;
      end if;
    end if;
  end process;

end rtl;
