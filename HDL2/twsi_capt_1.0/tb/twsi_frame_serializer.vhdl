library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity twsi_frame_serializer is
  generic (
    n_twsi_freq_div : natural := 2;
    n_data_bytes : natural := 8;
    n_data_msgid_bits : natural := 8;
    n_data_len_bits : natural := 8;
    n_data_seq_bits : natural := 8
    );
  port (
    clock : in std_logic;
    reset : in std_logic;
    tx_shift_en : in std_logic;
    tx_rq : in std_logic;
    tx_busy : out std_logic;
    --
    tx_msgid : in std_logic_vector (n_data_msgid_bits-1 downto 0);
    tx_seq : in std_logic_vector (n_data_seq_bits-1 downto 0);
    tx_data : in std_logic_vector ((n_data_bytes*8)-1 downto 0);
    --
    tx_do : out std_logic;
    tx_so : out std_logic
    );
end twsi_frame_serializer;

architecture rtl of twsi_frame_serializer is

  constant data_header : std_logic_vector (7 downto 0) := "11110101";
  constant data_msgid_idx : natural := data_header'length;
  constant data_seq_idx : natural := data_msgid_idx + n_data_msgid_bits;
  constant data_len_idx : natural := data_seq_idx + n_data_msgid_bits;
  constant data_payload_idx : natural := data_len_idx + n_data_seq_bits;
  constant data_frame_bits : natural := n_data_bytes * 8 + data_payload_idx;
  signal tx_busy_st : std_logic;
  signal tx_frame: std_logic_vector (data_frame_bits - 1 downto 0);
  signal tx_shr: std_logic_vector (data_frame_bits - 1 downto 0);
  signal tx_bit_cnt: integer range 0 to data_frame_bits - 1;
  signal tx_freq_div_cnt: integer range 0 to n_twsi_freq_div - 1;
  signal tx_do_st : std_logic;
  signal tx_so_st : std_logic;

begin

  tx_frame(data_header'length-1 downto 0) <= data_header;

  tx_frame(data_msgid_idx + n_data_msgid_bits-1 downto data_msgid_idx) <= tx_msgid;

  tx_frame(data_seq_idx + n_data_seq_bits-1 downto data_seq_idx) <=  tx_seq;

  tx_frame(data_len_idx + n_data_len_bits-1 downto data_len_idx) <=  std_logic_vector(to_unsigned(n_data_bytes, n_data_len_bits));

  tx_frame(data_frame_bits-1 downto data_payload_idx) <=  tx_data;

  -- Send data over two wire interface

  tx_process: process(clock, tx_rq, tx_frame, reset)
  begin
    if rising_edge(clock) then
      if reset = '1' then
        tx_bit_cnt <= 0;
        tx_busy_st <= '0';
        tx_do_st <= '0';
        tx_so_st <= '0';
      elsif tx_busy_st = '0' then
        tx_do_st <= '0';
        tx_so_st <= '0';
        tx_bit_cnt <= 0;
        if tx_rq = '1' then
          tx_do_st <= tx_frame(0);
          tx_so_st <= not tx_frame(0);
          tx_shr <= '0' & tx_frame(data_frame_bits - 1 downto 1);
          tx_busy_st <= '1';
          tx_freq_div_cnt <= n_twsi_freq_div - 1;
          tx_bit_cnt <= data_frame_bits - 1;
        end if;
      elsif tx_freq_div_cnt /= 0 then
        tx_freq_div_cnt <= tx_freq_div_cnt - 1;
      elsif (tx_bit_cnt /= 0) and (tx_shift_en = '1') then
        tx_freq_div_cnt <= n_twsi_freq_div - 1;
        if tx_do_st = tx_shr(0) then
          tx_so_st <= not tx_so_st;
        else
          tx_so_st <= tx_so_st;
        end if;
        tx_do_st <= tx_shr(0);
        tx_shr <= '0' & tx_shr(data_frame_bits - 1 downto 1);
        tx_bit_cnt <= tx_bit_cnt - 1;
      else
        tx_do_st <= '0';
        tx_so_st <= '0';
        tx_busy_st <= '0';
      end if;
    end if;
  end process;

  tx_do <= tx_do_st;
  tx_so <= tx_so_st;
  tx_busy <= tx_busy_st;

end architecture;
