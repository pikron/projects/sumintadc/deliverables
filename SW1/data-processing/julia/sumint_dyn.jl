using LsqFit
using Polynomials
using Statistics
using DSP
using DelimitedFiles

function arcov(x,M)
    N=length(x)
    xx=[x[:]; 0]
    X=zeros(N-M,M+1)
    for m=1:M+1
        X[:,m]=xx[m:(m+N-M-1)]
    end
    Xu=X[:,end:-1:1]
    XM=Xu./sqrt(N-M)
    Xc=XM[:,2:end];
    X1=XM[:,1];
    a=[1; -Xc\X1]
end

function guess_f(x)
    z = roots(Poly(arcov(x .- mean(x), 2)[end:-1:1]))
    dph0 = mean(abs.(angle.(z)))
    return dph0
end

@. swave(t, th) = th[4] .+ (th[2]*cos.(th[1]*t) + th[3]*sin.(th[1]*t))

function sinad(t, x, w0)
    A0 = std(x)/sqrt(2)
    DC0 = mean(x)
    opt = curve_fit(swave, t, x, [w0, A0, A0, DC0])
    x_f = swave(t, opt.param)
    e = x - x_f
    A_rms = hypot(opt.param[2], opt.param[3])
    e_rms = rms(e)
    SINADdB = 20*log10(A_rms/e_rms)

    return SINADdB, x_f, opt.param
end

function sumint_decode(x_samp, x_subs, Tclk, Tsamp)
    N = length(x_samp) - 1
    x_val = 2*Tclk*x_samp[2:end]./(Tsamp .+ Tclk*(x_subs[2:end] - x_subs[1:end-1]))
    t_com = (0:N-1)*Tsamp + Tclk*(x_subs[2:end] + x_subs[1:end-1])/2
    return t_com, x_val
end

###
adc=readdlm("./sumintadc-char-lcl/220219/dynamic_channel0_blind_channel1/dynamic_meas_16/adc_data.csv", ',', skipstart=1)
x_samp = adc[:,1]
x_subs = adc[:,2]
N = length(x_samp)
t = float.(0:N-1)

# SumInt parameters
Tclk = 1/200e6
n_pwm = 50
Tmod = 1/200e3
Tsamp = Tmod*n_pwm

# SumInt subsample processing
#x_val = x_samp*Tclk/Tsamp
#t_com = t*Tsamp
t_com, x_val = sumint_decode(x_samp, x_subs, Tclk, Tsamp)

#x = 50e3*x_val # Pysvejc
#t_x = t_com/Tsamp

# sinewave fit
w0 = guess_f(x_val)/Tsamp
SINADdB, x_f, th = sinad(t_com, x_val, w0)

clf();plot(t_com, [x_val x_f x_val-x_f],".-");
@show SINADdB

###
n_m = 42
A_rms = zeros(n_m)
e_rms = zeros(n_m)
SINADdB = zeros(n_m)

for m = 0:n_m-1
    @show m
    #adc = readdlm("./sumintadc-char-lcl/220219/dynamic_channel0_blind_channel1/dynamic_meas_"*string(m)*"/adc_data.csv", ',', skipstart=1)
    adc = readdlm("./sumintadc-char-lcl/220219/dynamic_channel3_blind_channel2/dynamic_meas_"*string(m)*"/adc_data.csv", ',', skipstart=1)

    t_com, x_val = sumint_decode(adc[:,1], adc[:,2], Tclk, Tsamp)
    w0 = guess_f(x_val)/Tsamp
    SINADdB[m+1], x_f, th = sinad(t_com, x_val, w0)

    A_rms[m+1] = hypot(th[2], th[3])
    e_rms[m+1] = rms(x_val - x_f)
end

begin
    clf()
    subplot(2,1,1)
    plot(reshape(SINADdB, 14, 3)[1:end-1,:], ".-")
    grid(true, axis=:y); ylim(70, 90)
    ylabel("SINAD [dB]")
    xticks(0:12); xlim(0,13)

    subplot(2,1,2)
    plot(reshape(e_rms, 14, 3)[1:end-1,:], ".-")
    grid(true, axis=:y);
    ylabel("e_{rms} = RMS(x - x_fit) [FS]")
    xlabel("meas no. mod 14")
    xticks(0:12); xlim(0,13); ylim(0, 1e-4);
end
