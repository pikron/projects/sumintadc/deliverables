"""
    Automated testing of the SumIntADC

    adc_static.py - master script for static tests of the ADC

    organization: Czech Technical University in Prague, Faculty of Electrical Engineering, Department of Measurement
    author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz
    advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz
    funded by PiKRON s.r.o. - http://www.pikron.com
"""

from keysightb2912a import KeysightB2912A
from agilent3458a import Agilent3458A
from agilent34410 import Agilent34410
from heat_chamber import HeatChamber
import constants as const

from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation as fa
import numpy as np
from datetime import datetime as dt
import pandas as pd
import time
from os import path, getcwd, mkdir
from progress.bar import Bar
from multiprocessing import Value, Lock, Process, Condition
from ctypes import c_bool
import socket

class AdcTester:
    def __init__(self):
        self.dmm_sig = Agilent3458A('GPIB0::10::INSTR')
        self.dmm_ref = Agilent3458A('GPIB0::22::INSTR')
        self.dsm = KeysightB2912A('GPIB0::23::INSTR')
        self.tm = Agilent34410('GPIB0::1::INSTR')
        self.hm = HeatChamber()
        self.dmm_sig.setup_for_dc_measurement(range=1)
        self.dmm_ref.setup_for_dc_measurement(range=10)

        self.measured_voltage = Value('f', 0)
        self.new_data = Value(c_bool, False)
        self.plt_lock = Condition()

    def get_unique_file_name(self, file_path, base_name):
        file_indx = 0
        file_name = base_name % (file_indx)
        while path.isfile(path.join(file_path, file_name)):
            file_name = base_name % (file_indx)
            file_indx += 1
        return file_name

    def get_unique_dir_name(self, dir_path, base_name):
        dir_indx = 0
        dir_name = base_name % (dir_indx)
        while path.isdir(path.join(dir_path, dir_name)):
            dir_indx += 1
            dir_name = base_name % (dir_indx)
        return dir_name

    def save_meas_info(self, start_voltage, stop_voltage, voltage_increment, settle_time, n_o_s, n_o_p, temperature, humidity):
        file_name = self.get_unique_file_name(path.join(getcwd(), "measurement_data"), "measurement_info_%d.txt")
        file_path = path.join(getcwd(), "measurement_data", file_name)
        message_str =   "%s\n\
                        \rtemperature: %f °C\n\
                        \rhumidity: %f %%\n\
                        \rchannel: %d \n\
                        \rblind channel: %d \n\
                        \rstart voltage: %f V\n\
                        \rstop voltage: %f V\n\
                        \rvoltage increment: %f V\n\
                        \rdelay between stages: %f s\n\
                        \rnumber of stages: %d\n\
                        \rnumber of points in one stage: %d\n\
                        \rdata file name: %s\n\
                        \rraw data folder name: %s\n\
                        \radc measurement argument: %s" % (str(dt.now()), temperature, humidity, const.CHANNEL, const.BLIND_CHANNEL, start_voltage, stop_voltage, voltage_increment, settle_time, n_o_s, n_o_p, path.basename(self.file_path), self.dir_name, const.CMD_ARG.decode('utf-8').rstrip("\n"))
        f = open(file_path, "w")
        f.write(message_str)
        f.close()

    def init_csv_file(self):
        self.raw_data_cnt = 0
        if path.isdir(path.join(getcwd(), "measurement_data")) != True:
            mkdir(path.join(getcwd(), "measurement_data"))
        self.dir_name = self.get_unique_dir_name(path.join(getcwd(), "measurement_data"), "raw_data_%d")
        mkdir(path.join(getcwd(), "measurement_data", self.dir_name))
        file_name = self.get_unique_file_name(path.join(getcwd(), "measurement_data"), "measured_values_%d.csv")
        self.file_path = path.join(getcwd(), "measurement_data", file_name)
        export_format = {'time': [], 'reference': [], 'input_signal': [], 'adc_avg': [], 'adc_std': [], 'adc_blind': [], 'multimeter_temperature': [], 'pt100_temperature': []}
        df = pd.DataFrame(export_format, columns=['time', 'reference', 'input_signal', 'adc_avg', 'adc_std', 'adc_blind', 'multimeter_temperature', 'pt100_temperature'])
        df.to_csv(self.file_path, index = False, header=True)

    def save_sample(self, timestamp_data, measured_data, reference_data, adc_data, adc_std, adc_blind, mm_temperature, pt100_temperature):
        export_format = {'time': [timestamp_data],  'reference': [reference_data], 'input_signal': [measured_data], 'adc_avg': [adc_data], 'adc_std': [adc_std], 'adc_blind': [adc_blind], 'multimeter_temperature': [mm_temperature], 'pt100_temperature': [pt100_temperature]}
        df = pd.DataFrame(export_format, columns=['time', 'reference', 'input_signal', 'adc_avg', 'adc_std', 'adc_blind', 'multimeter_temperature', 'pt100_temperature'])
        df.to_csv(self.file_path, index=False, header=False, mode='a')

    def save_adc_data(self, data, subsample_data, voltage):
        file_name = "raw_data_%d.csv" % self.raw_data_cnt
        self.raw_data_cnt += 1
        file_path = path.join(getcwd(), "measurement_data", self.dir_name, file_name)
        export_format = {'values': data, 'subsample_values': subsample_data}
        df = pd.DataFrame(export_format, columns=['values', 'subsample_values'])
        df.to_csv(file_path, index=False, header=True)

    def voltage_sweep(self, start_voltage, stop_voltage, num_of_stg, num_of_pts, settle_time, temperature, humidity):
        self.init_csv_file()
        progress_bar = Bar('Measuring', max=num_of_stg * num_of_pts)
        values_set = np.linspace(start_voltage, stop_voltage, num_of_stg)
        self.save_meas_info(start_voltage, stop_voltage, values_set[-1] - values_set[-2], settle_time, num_of_stg, num_of_pts, temperature, humidity)
        self.dsm.out_enable(True)
        for val in values_set:
            self.dsm.set_voltage(val)
            time.sleep(settle_time)
            for _ in range(0, num_of_pts):
                # time.sleep(0.1)
                self.dmm_sig.get_dcv_w()
                self.dmm_ref.get_dcv_w()
                adc_mean = None
                while adc_mean is None:
                    adc_mean, adc_std, adc_blind = self.grab_adc_data(2 * const.CHANNEL, 2 * const.BLIND_CHANNEL, val)
                measured_value = float(self.dmm_sig.get_dcv_r())
                reference_value = float(self.dmm_ref.get_dcv_r())
                value_timestamp = dt.now()
                self.save_sample(value_timestamp, measured_value, reference_value, adc_mean, adc_std, adc_blind, self.dmm_sig.get_temp(), self.tm.read_temp())
                self.plot_sample(value_timestamp, measured_value)
                progress_bar.next()
        progress_bar.finish()
        self.dsm.out_enable(False)

    def grab_adc_data(self, channel, blind_channel, voltage_lvl):
        self.sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sck.connect(const.ADC_ADDRESS)
        self.sck.sendall(const.CMD_ARG)
        receive_ready = self.sck.makefile('r')
        line = receive_ready.readline().rstrip("\n")
        if len(line) == 0:
            print("No response from ADC")
            return None, None, None
        first = True
        while True:
            line = receive_ready.readline().rstrip("\n")
            if len(line) == 0:
                break
            value_sample = np.asarray(line.split("\t"), dtype=np.double)
            value_sample = np.reshape(value_sample, (1, len(value_sample)))
            if first:
                first = False
                value_series = value_sample
            else:
                value_series = np.append(value_series, value_sample, axis=0)
        self.sck.close()

        if first:
            return None, None, None

        if np.shape(value_series)[0] < 3000:
            return None, None, None
        else:
            self.save_adc_data(value_series[:, channel], value_series[:, channel + 1], voltage_lvl)
            if const.BLIND_CHANNEL is not None:
                return np.mean(value_series, axis=0)[channel], np.std(value_series, axis=0)[channel], np.mean(value_series, axis=0)[blind_channel]
            else:
                return np.mean(value_series, axis=0)[channel], np.std(value_series, axis=0)[channel], 0

    def plot_sample(self, timestamp_data, measured_data):
        self.plt_lock.acquire()
        self.measured_voltage.value = measured_data
        self.new_data.value = True
        self.plt_lock.release()

def plotting(plt_lock, measured_voltage, new_data, meas_label):
    def update(frame):
        plt_lock.acquire()
        if new_data.value == True:
            new_data.value = False
            x.append(dt.now())
            y.append(measured_voltage.value)
            line.set_data(x, y)
            figure.gca().relim()
            figure.gca().autoscale_view()
        plt_lock.release()
        return line,

    x, y = [], []
    figure = plt.figure(figsize=(8, 6))
    line, = plt.plot_date(x, y, '-o')
    plt.xlabel("t")
    plt.ylabel("U [V]")
    plt.title(meas_label)
    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.2)
    plt.grid(b=True, which='minor', color='k', linestyle='--', alpha=0.1)
    plt.minorticks_on()

    animation = fa(figure, update, interval=200)
    plt.show()

if __name__ == "__main__":
    adc_test = AdcTester()

    if const.USE_PLOT:
        meas_label = "start: %f V, stop: %f V\nnumber of stages: %d\nnumber of points in stage: %d" % (const.START_VOLTAGE, const.END_VOLTAGE, const.NUMBER_OF_STAGES, const.NUMBER_OF_POINTS_IN_STAGE)
        th_plot = Process(target=plotting, args=(adc_test.plt_lock, adc_test.measured_voltage, adc_test.new_data, meas_label), daemon=True)
        th_plot.start()

    if const.ENV_CONDITIONS is not None:
        adc_test.hm.set_manual_mode(True)
        for temp, hum in const.ENV_CONDITIONS:
            adc_test.hm.set_temperature_setpoint(temp)
            adc_test.hm.set_humidity_setpoint(hum)
            while adc_test.hm.values_in_limits(1, 30) == False:
                print("Preparing desired conditions", end="\r")
            print("\nTemperature settling")
            time.sleep(const.TEMPERATURE_SETTLE_TIME)
            adc_test.voltage_sweep(const.START_VOLTAGE, const.END_VOLTAGE, const.NUMBER_OF_STAGES, const.NUMBER_OF_POINTS_IN_STAGE, const.SETTLE_TIME, temp, hum)
            adc_test.voltage_sweep(const.END_VOLTAGE, const.START_VOLTAGE, const.NUMBER_OF_STAGES, const.NUMBER_OF_POINTS_IN_STAGE, const.SETTLE_TIME, temp, hum)
        adc_test.hm.set_manual_mode(False)
    else:
        adc_test.voltage_sweep(const.START_VOLTAGE, const.END_VOLTAGE, const.NUMBER_OF_STAGES, const.NUMBER_OF_POINTS_IN_STAGE, const.SETTLE_TIME, adc_test.hm.get_actual_temperature(), adc_test.hm.get_actual_humidity())
        adc_test.voltage_sweep(const.END_VOLTAGE, const.START_VOLTAGE, const.NUMBER_OF_STAGES, const.NUMBER_OF_POINTS_IN_STAGE, const.SETTLE_TIME, adc_test.hm.get_actual_temperature(), adc_test.hm.get_actual_humidity())
