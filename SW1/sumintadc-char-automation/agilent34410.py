"""
    Automated testing of the SumIntADC

    agilent34410.py - control script for DMM Agilent 34410A

    organization: Czech Technical University in Prague, Faculty of Electrical Engineering, Department of Measurement
    author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz
    advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz
    funded by PiKRON s.r.o. - http://www.pikron.com
"""

import pyvisa as pv

class Agilent34410:
    def __init__(self, gpib_name):
        rm = pv.ResourceManager()
        self.rd = rm.open_resource(gpib_name)
        self.rd.write_termination = '\n'
        self.rd.read_termination = '\n'

    def read_temp(self):
        return float(self.rd.query('READ?'))

    def get_id(self):
        return self.rd.query('*IDN?')

if __name__ == "__main__":
    mm = Agilent34410('GPIB0::1::INSTR')
