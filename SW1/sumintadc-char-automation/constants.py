"""
    Automated testing of the SumIntADC

    constants.py - constants definition for measurement

    organization: Czech Technical University in Prague, Faculty of Electrical Engineering, Department of Measurement
    author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz
    advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz
    funded by PiKRON s.r.o. - http://www.pikron.com
"""

ADC_ADDRESS = ('192.168.1.125', 5577)
CHAMBER_ADDRESS = ('192.168.1.198', 2049)
START_VOLTAGE = -1.1
END_VOLTAGE = 1.1
NOM_MIN_VOLTAGE = -1.0
NOM_MAX_VOLTAGE = 1.0
NUMBER_OF_STAGES = 11 * 100 + 1
NUMBER_OF_POINTS_IN_STAGE = 3
SETTLE_TIME = 1
ENV_CONDITIONS = [(85, 5), (40, 5), (25, 20), (0, 50), (-40, 55)]
#ENV_CONDITIONS = None
CHANNEL = 0
BLIND_CHANNEL = 1
USE_PLOT = True
TEMPERATURE_SETTLE_TIME = 4 * 60
CMD_ARG = b"capture channel_mask=0x3f decimation=50\n"
NUM_OF_DMM_READINGS = 37880 / 2
AMPLITUDE = 2.0
FREQUENCY_SET = [10, 15, 22, 33, 47, 68, 100, 150, 220, 330, 470, 680, 1000, 15000]
