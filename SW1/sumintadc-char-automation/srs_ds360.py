"""
    Automated testing of the SumIntADC

    srs_ds360.py - control script for low distortion function generator SRS DS360

    organization: Czech Technical University in Prague, Faculty of Electrical Engineering, Department of Measurement
    author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz
    advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz
    funded by PiKRON s.r.o. - http://www.pikron.com
"""

import pyvisa as pv

class SrsDs360:
    def __init__(self, gpib_name):
        rm = pv.ResourceManager()
        self.rd = rm.open_resource(gpib_name)
        self.rd.write_termination = '\n'
        self.rd.read_termination = '\n'

    def dev_reset(self):
        self.rd.write('*RST')

    def get_id(self):
        return self.rd.query('*IDN?')

    def set_sine_function(self):
        self.rd.write('FUNC 0')

    def set_amplitude(self, amplitude):
        self.rd.write('AMPL' + str(amplitude) + 'VP')

    def set_frequency(self, frequency):
        self.rd.write('FREQ ' + str(frequency))

    def set_offset(self, offset):
        self.rd.write('OFFS ' + str(offset))

    def enable_output(self, enable):
        if enable:
            self.rd.write('OUTE 1')
        else:
            self.rd.write('OUTE 0')

if __name__ == "__main__":
    fg = SrsDs360('GPIB0::8::INSTR')
