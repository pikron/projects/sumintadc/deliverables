"""
    Automated testing of the SumIntADC

    heat_chamber.py - control script for heat chamber

    organization: Czech Technical University in Prague, Faculty of Electrical Engineering, Department of Measurement
    author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz
    advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz
    funded by PiKRON s.r.o. - http://www.pikron.com
"""

import socket
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from time import sleep
from datetime import datetime as dt
import constants as const

class HeatChamber:
    def __init__(self):
        self.delim = b'\xb6'
        self.cr = b'\r'
        self.sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sck.connect(const.CHAMBER_ADDRESS)

    def compose_message(self, cmd_id, ch_id, arg_1="", arg_2="", arg_3="", arg_4=""):
        message = cmd_id + self.delim + ch_id
        if arg_1 != "":
            message = message + self.delim + arg_1
        if arg_2 != "":
            message = message + self.delim + arg_2
        if arg_3 != "":
            message = message + self.delim + arg_3
        if arg_4 != "":
            message = message + self.delim + arg_4
        return message + self.delim + self.cr

    def read_response(self):
        recv_file = self.sck.makefile('r')
        line = recv_file.readline()
        if len(line) != 0:
            return line.rstrip("\n")
        else:
            return None

    def get_sys_type(self):
        self.sck.sendall(self.compose_message(b'99997', b'1', b'1'))
        return self.read_response()

    def set_manual_mode(self, apply=True):
        self.sck.sendall(self.compose_message(b'14001', b'1', b'1', str(apply).encode('UTF-8')))
        return self.read_response()

    def set_temperature_setpoint(self, temperature):
        self.sck.sendall(self.compose_message(b'11001', b'1', b'1', str(temperature).encode('UTF-8')))
        return self.read_response()

    def set_humidity_setpoint(self, humidity):
        self.sck.sendall(self.compose_message(b'11001', b'1', b'2', str(humidity).encode('UTF-8')))
        return self.read_response()

    def get_temperature_setpoint(self):
        self.sck.sendall(self.compose_message(b'11002', b'1', b'1'))
        return float(self.read_response()[2:])

    def get_humidity_setpoint(self):
        self.sck.sendall(self.compose_message(b'11002', b'1', b'2'))
        return float(self.read_response()[2:])

    def get_actual_temperature(self):
        self.sck.sendall(self.compose_message(b'11004', b'1', b'1'))
        return float(self.read_response()[2:])

    def get_actual_humidity(self):
        self.sck.sendall(self.compose_message(b'11004', b'1', b'2'))
        return float(self.read_response()[2:])

    def temperature_in_limits(self, deviation=1):
        if abs(self.get_actual_temperature() - self.get_temperature_setpoint()) < deviation:
            return True
        else:
            return False

    def humidity_in_limits(self, deviation=1):
        if abs(self.get_actual_humidity() - self.get_humidity_setpoint()) < deviation:
            return True
        else:
            return False

    def values_in_limits(self, temperature_deviation=1, humidity_deviation=1):
        if self.temperature_in_limits(temperature_deviation) and self.humidity_in_limits(humidity_deviation):
            return True
        else:
            return False

    def report_conditions(self):
        while(True):
            print("temperature: %2.2f °C, humidity: %2.2f %%" % ((self.get_actual_temperature(), self.get_actual_humidity())), end='\r')
            sleep(1)

    def plot_conditions(self):
        self.act_time, self.act_temp, self.act_hum = [], [], []
        self.figure_temp = plt.figure(1)
        self.line_temp, = plt.plot_date(self.act_time, self.act_temp, '-o')
        plt.xlabel("time [-]")
        plt.ylabel("t [°C]")
        plt.title('Chamber temperature')
        plt.grid(visible=True, which='major', color='k', linestyle='-', alpha=0.2)
        plt.grid(visible=True, which='minor', color='k', linestyle='--', alpha=0.1)
        plt.minorticks_on()

        self.figure_hum = plt.figure(2)
        self.line_hum, = plt.plot_date(self.act_time, self.act_hum, '-o')
        plt.xlabel("time [-]")
        plt.ylabel("RH [%]")
        plt.title('Chamber humidity')
        plt.grid(visible=True, which='major', color='k', linestyle='-', alpha=0.2)
        plt.grid(visible=True, which='minor', color='k', linestyle='--', alpha=0.1)
        plt.minorticks_on()

        animation_temp = FuncAnimation(self.figure_temp, self.update_temp, interval=1000)
        animation_hum = FuncAnimation(self.figure_hum, self.update_hum, interval=1000)
        plt.show()

    def update_temp(self, frame):
        self.act_time.append(dt.now())
        self.act_temp.append(self.get_actual_temperature())
        self.act_hum.append(self.get_actual_humidity())
        print("temperature: %2.2f °C, humidity: %2.2f %%" % (self.act_temp[-1], self.act_hum[-1]), end='\r')

        self.line_temp.set_data(self.act_time, self.act_temp)
        self.figure_temp.gca().relim()
        self.figure_temp.gca().autoscale_view()
        return self.line_temp,

    def update_hum(self, frame):
        self.line_hum.set_data(self.act_time, self.act_hum)
        self.figure_hum.gca().relim()
        self.figure_hum.gca().autoscale_view()
        return self.line_hum,

if __name__ == "__main__":
    hm = HeatChamber()
