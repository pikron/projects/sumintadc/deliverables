"""
    Automated testing of the SumIntADC

    keysight2912a.py - control script for precision source/measure unit Keysight B2912A

    organization: Czech Technical University in Prague, Faculty of Electrical Engineering, Department of Measurement
    author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz
    advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz
    funded by PiKRON s.r.o. - http://www.pikron.com
"""

import pyvisa as pv

class KeysightB2912A:
    def __init__(self, gpib_name):
        rm = pv.ResourceManager()
        self.rd = rm.open_resource(gpib_name)
        self.rd.write_termination = '\n'
        self.rd.read_termination = '\n'
        self.dev_reset()
        self.set_voltage_mode()

    def dev_reset(self):
        self.rd.write('*RST')

    def get_id(self):
        return self.rd.query('*IDN?')

    def set_voltage_mode(self):
        self.rd.write(':SOUR:FUNC:MODE VOLT')

    def set_voltage(self, voltage_value):
        self.rd.write(':SOUR:VOLT ' + str(voltage_value))

    def out_enable(self, enable=True):
        if enable:
            self.rd.write(':OUTP ON')
        else:
            self.rd.write(':OUTP OFF')

if __name__ == "__main__":
    sm = KeysightB2912A('GPIB0::23::INSTR')
