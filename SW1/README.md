# Implementation of Sum-Int ADC IP-core into radiation-tolerant FPGA and performance evaluation
# SW1 – Final Test Software for Linux Host System for Sum-Int ADC Control and Data Acquisition

* [sumit-twsi-decode](sumit-twsi-decode)
  * application to decode recorded stream of variable number of Sum-Int ADC serialized samples
* [twsi-capture](twsi-capture)
  * application to receive data send from nanoXplore NX1H35AS-EK kit on Xilinx Zynq or other FPGA platform
  * the Sum-Int ADC is implemented by [HDL2/sumint_nx_bb_v3/siroladc.vhdl](../HDL2/sumint_nx_bb_v3/src/siroladc.vhdl) component
  * Two Wire Serial Interface (TWSI - simple unidirectional link with Space-Wire like bits encoding method) is implemented in design top level [HDL2/sumint_nx_bb_v3/src](../HDL2/sumint_nx_bb_v3/src)
  * TWSI data reception on Xilinx Zynq side is implemented in Vivado component [HDL2/twsi_capt_1.0](../HDL2/twsi_capt_1.0)
* [sumintadc-char-automation](sumintadc-char-automation)
  * Python application to control metrological laboratory setup automatic Sum-Int characterization 
* [data-processing](data-processing)
  * Python scripts to copute range of errors and parameters against linear approximations at 25°C
  * Julia language script to compute SINAD
