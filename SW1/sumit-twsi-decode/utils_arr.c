/**
 * si_add_to_arr - adds elements to the dynamically growing array
 * @ps:		Pointer to character pointer into parsed string
 * @pdata:	Pointer to the data array pointer location pointer
 * @plen:	Pointer to location, where length of already accumulated
 *		in bytes is held
 * @base:	Base of converted number, 0 means select by C rules
 * @elsize:	Size of one array element
 *
 * Function reads digits and converts them to longint values.
 * It stops at end of the string or if some of stop charracters is reached.
 * Returns 1 for successful conversion or -1 if no character can be
 * converted. Pointer *@ps paints after last character belonging to
 * numeric input.
 */

#include <string.h>
#include <malloc.h>
#include "utils.h"

int si_add_to_arr(char **ps, void **pdata,int *plen,int base, int elsize, char *stop_chars)
{
  char *s=*ps;
  long val;
  unsigned char *p;
  int i;
  int elements=0;
  const int endian_chk=1;
  if(!stop_chars)
    stop_chars="";

  if(!elsize)
    elsize=1;
  if(elsize>sizeof(long))
    return -1;
 
  do{
    while(1){
      if(!*s || strchr(stop_chars,*s)){
        *ps=s;
        return elements;
      }
      if(!strchr(", \t:;",*s))
        break;
      s++;
    }
    if(si_long(&s,&val,base)<0){
      *ps=s;
      return -1;
    }
    if(*pdata==NULL){
      *plen=0;
      *pdata=p=malloc(elsize);
    }else{
      p=realloc(*pdata,*plen+elsize);
      if(p==NULL) return -1;
      *pdata=p;
    }
   
    p+=*plen;
    if(*(char*)&endian_chk){
      for(i=elsize;i--;){
	*(p++)=val;
	val>>=8;
      }
    }else{
      p+=elsize;
      for(i=elsize;i--;){
	*(--p)=val;
	val>>=8;
      }
    }
    (*plen)+=elsize;
    elements++;
  } while(1);
}
