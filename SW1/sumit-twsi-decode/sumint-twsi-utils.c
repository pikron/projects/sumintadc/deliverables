/*******************************************************************

  Simple code to decode SumInt ADC data from TWSI capture

  Base on rdwrmem.c, Copyright 2004 - 2017 by Pavel Pisa

  (C) Copyright 2004 - 2022 by Pavel Pisa
      e-mail:   ppisa@pikron.com
      project:  https://gitlab.com/pikron/projects/sumintadc/
      personal: http://cmp.felk.cvut.cz/~pisa
      work:     http://www.pikron.com/
      license:  any combination GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <malloc.h>
#include <string.h>
#include <byteswap.h>
#include <getopt.h>
#include <inttypes.h>

#include "sumint-twsi.h"
#include "mmap_file.h"


int sumint_twsi_raw_open(sumint_twsi_raw_t *twsiraw, const char *fname)
{
  twsiraw->frames_first = NULL;
  twsiraw->frames_end = NULL;
  twsiraw->frame_size = 0;
  twsiraw->frame_count = 0;
  twsiraw->mem_start = NULL;

  twsiraw->mfr = mmap_file(fname, 0, 0);

  if (twsiraw->mfr == NULL)
    return -1;

  twsiraw->mem_start = twsiraw->mfr->data_start;
  twsiraw->frames_end = (char *) twsiraw->mfr->data_start +
                        twsiraw->mfr->data_size;

  return 0;
}

int sumint_twsi_in_memory(sumint_twsi_raw_t *twsiraw, void *mem_start, size_t mem_size)
{
  twsiraw->frames_first = NULL;
  twsiraw->frames_end = NULL;
  twsiraw->frame_size = 0;
  twsiraw->frame_count = 0;
  twsiraw->mem_start = NULL;
  twsiraw->mfr = NULL;

  twsiraw->mem_start = mem_start;
  twsiraw->frames_end = (char *)mem_start + mem_size;
  return 0;
}

int sumint_twsi_check_frame(void *frame, void *frames_end, int frames_try)
{
  uint8_t *p;
  uint8_t len;

  p = (uint8_t *)frame;

  if ((uint8_t *)frames_end - p < SUMIT_TWSI_DATA_o)
    return 0;

  do {
    if (p[SUMIT_TWSI_HEADER_o] != SUMIT_TWSI_HEADER_MAGIC)
      return -1;
    if (p[SUMIT_TWSI_MSGID_o] != SUMIT_TWSI_MSGID_ADC)
      return -1;
    len = p[SUMIT_TWSI_LEN_o];
    if ((uint8_t *)frames_end - p < len + SUMIT_TWSI_DATA_o)
      if (p == frame)
        return 0;
    p = p + SUMIT_TWSI_DATA_o + len;
  } while(((uint8_t *)frames_end - p >= SUMIT_TWSI_DATA_o) &&
          frames_try--);

  return 1;
}

int sumint_twsi_raw_setup_frames(sumint_twsi_raw_t *twsiraw, int validate)
{
  uint8_t *p;
  uint8_t len;
  int gues_try;

  if (twsiraw->mem_start == NULL)
    return -1;

  p = (uint8_t *)twsiraw->mem_start;
  for (gues_try = 1000 ; ((uint8_t *)twsiraw->frames_end - p >= 4) &&
       gues_try ; p++, gues_try--) {
    if (sumint_twsi_check_frame(p, twsiraw->frames_end, 32) <= 0)
      continue;
    twsiraw->frames_first = p;
    len = p[SUMIT_TWSI_LEN_o];
    twsiraw->frame_size = SUMIT_TWSI_DATA_o + len;
    twsiraw->frame_count = ((uint8_t *)twsiraw->frames_end - p) /
                           twsiraw->frame_size;
    return twsiraw->frame_count;
  }
  return -1;
}

int sumint_twsi_raw_values_print(FILE *file, sumint_twsi_raw_t *twsiraw, unsigned int mask)
{
  uint8_t *p = (uint8_t *)twsiraw->frames_first;
  uint8_t *r = NULL;
  uint8_t *s = NULL;
  unsigned int m;
  unsigned int val;
  unsigned int frame_count = 0;
  int res;
  int tab;

  for( ; p + SUMIT_TWSI_DATA_o <= (uint8_t *)twsiraw->frames_end;
      r = p, p += twsiraw->frame_size) {
    res = sumint_twsi_check_frame(p, twsiraw->frames_end, 1);
    if (res < 0) {
      fprintf(stderr, "TWSI decode error at position %ld\n",
              (long) (p - (uint8_t *)twsiraw->frames_first));
      return -1;
    }
    if (!res)
      break;
    if (r == NULL)
      continue;

    if ((uint8_t)(p[SUMIT_TWSI_SEQ_o] - r[SUMIT_TWSI_SEQ_o]) != 1) {
      fprintf(stderr, "TWSI seq order broken 0x%02x 0x%02x at %ld\n",
              r[SUMIT_TWSI_SEQ_o], p[SUMIT_TWSI_SEQ_o],
              (long) (p - (uint8_t *)twsiraw->frames_first));
      return -2;
    }

    r = r + SUMIT_TWSI_DATA_o;
    s = p + SUMIT_TWSI_DATA_o;
    tab = 0;
    for (m = mask; (s - p < twsiraw->frame_size) && m;
         m >>= 1, r += 2, s += 2) {
      if (!(m & 1))
        continue;
      val = (uint16_t)(sumint_twsi_get_u16_le(s) - sumint_twsi_get_u16_le(r));

      if (tab)
        printf("\t");
      //printf("0x%04x", sumint_twsi_get_u16_le(r));
      fprintf(file, "%u", val);
      tab = 1;
    }
    fprintf(file, "\n");
    frame_count += 1;
  }

  return frame_count;
}

int sumint_twsi_filtered_values_print(FILE *file, sumint_twsi_raw_t *twsiraw, unsigned int mask, int decimation, int options)
{
  uint8_t *p = (uint8_t *)twsiraw->frames_first;
  uint8_t *r = NULL;
  uint8_t *s = NULL;
  unsigned int m;
  unsigned int val;
  unsigned int rem;
  unsigned int frame_count = 0;
  int res;
  int tab;
  int sep_char = (options & SUMINT_TWSI_PRTOPT_CSV) ? ',': '\t';

  for( ; p + SUMIT_TWSI_DATA_o <= (uint8_t *)twsiraw->frames_end;
      r = p, p += twsiraw->frame_size * decimation) {
    res = sumint_twsi_check_frame(p, twsiraw->frames_end, 1);
    if (res < 0) {
      fprintf(stderr, "TWSI decode error at position %ld\n",
              (long) (p - (uint8_t *)twsiraw->frames_first));
      return -1;
    }
    if (!res)
      break;
    if (r == NULL)
      continue;

    if ((uint8_t)(p[SUMIT_TWSI_SEQ_o] - r[SUMIT_TWSI_SEQ_o]) != decimation) {
      fprintf(stderr, "TWSI seq order broken 0x%02x 0x%02x at %ld\n",
              r[SUMIT_TWSI_SEQ_o], p[SUMIT_TWSI_SEQ_o],
              (long) (p - (uint8_t *)twsiraw->frames_first));
      return -2;
    }

    r = r + SUMIT_TWSI_DATA_o;
    s = p + SUMIT_TWSI_DATA_o;
    tab = 0;
    for (m = mask; (s - p < twsiraw->frame_size) && m;
         m >>= 1, r += 2, s += 2) {
      if (!(m & 1))
        continue;

      val = (uint16_t)(sumint_twsi_get_u16_le(s) - sumint_twsi_get_u16_le(r));
      rem = (uint16_t)(sumint_twsi_get_u16_le(s) - sumint_twsi_get_u16_le(s - twsiraw->frame_size));

      if (tab) {
        fprintf(file, "%c", sep_char);
      }
      //printf("0x%04x", sumint_twsi_get_u16_le(r));
      fprintf(file, "%u%c%u", val, sep_char, rem);
      tab = 1;
    }
    fprintf(file, "\n");
    frame_count += 1;
  }

  return frame_count;
}

