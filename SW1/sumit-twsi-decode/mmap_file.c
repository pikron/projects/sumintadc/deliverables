/*******************************************************************
  Simple program to check LCD functionality on MicroZed
  based MZ_APO board designed by Petr Porazil at PiKRON

  mzapo_phys.h      - mapping of the physical address to process

  (C) Copyright 2017 by Pavel Pisa
      e-mail:   pisa@cmp.felk.cvut.cz
      homepage: http://cmp.felk.cvut.cz/~pisa
      company:  http://www.pikron.com/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mmap_file.h"

mmaped_file_region_t *mmap_file_region(int fd, off_t region_base,
                                       ssize_t region_size, int rdwr)

{
  size_t map_size;
  unsigned long pagesize;
  unsigned char *mm;
  unsigned char *mem;
  mmaped_file_region_t *mfr;

  pagesize=sysconf(_SC_PAGESIZE);

  if (region_size == -1) {
    struct stat fst;
    if (fstat(fd, &fst) == -1)
      return NULL;
    region_size = fst.st_size;
  }

  map_size = ((region_base & (pagesize-1)) + region_size + pagesize-1) & ~(pagesize-1);

  mm = (unsigned char *)mmap(NULL, map_size,
              (rdwr? PROT_WRITE: 0)| PROT_READ,
              MAP_SHARED, fd, region_base & ~(pagesize-1));
  mem = mm + (region_base & (pagesize-1));

  if (mm == MAP_FAILED) {
    return NULL;
  }

  mfr = malloc(sizeof(mmaped_file_region_t));
  if (mfr == NULL) {
    munmap(mm, map_size);
    return NULL;
  }

  mfr->data_start = mem;
  mfr->data_size = region_size;
  mfr->map_start = mm;
  mfr->map_size = map_size;
  mfr->region_base = region_base;

  return mfr;
}

mmaped_file_region_t *mmap_file(const char *fname, int opt_sync, int rdwr)
{ 
  int fd;

  fd = open(fname, (rdwr? O_RDWR: O_RDONLY ) | (opt_sync? O_SYNC: 0));
  if (fd == -1) {
    return NULL;
  }
  return mmap_file_region(fd, 0, -1, rdwr);
}
