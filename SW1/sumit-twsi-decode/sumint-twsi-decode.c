/*******************************************************************

  Simple code to decode SumInt ADC data from TWSI capture

  Base on rdwrmem.c, Copyright 2004 - 2017 by Pavel Pisa

  (C) Copyright 2004 - 2022 by Pavel Pisa
      e-mail:   ppisa@pikron.com
      project:  https://gitlab.com/pikron/projects/sumintadc/
      personal: http://cmp.felk.cvut.cz/~pisa
      work:     http://www.pikron.com/
      license:  any combination GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <malloc.h>
#include <string.h>
#include <byteswap.h>
#include <getopt.h>
#include <inttypes.h>

#include "sumint-twsi.h"
#include "mmap_file.h"
#include "utils.h"

char *twsi_fname="log-file.twsi";


int mem_dump(void *buf, unsigned long start, unsigned long len, int blen)
{
  unsigned long addr=start;
  volatile unsigned char *p=buf;
  int i;

  while(len){
    printf("%08lX:",addr);
    i=len>16?16:len;
    addr+=i;
    len-=i;
    while(i>0){
      switch(blen){
	case 4:
	  i-=4;
          printf("%08X%c",*(volatile uint32_t*)p,i>0?' ':'\n');
	  p+=4;
	  break;
	case 2:
	  i-=2;
          printf("%04X%c",*(volatile uint16_t*)p,i>0?' ':'\n');
	  p+=2;
	  break;
	default:
	  i--;
          printf("%02X%c",*(volatile uint8_t*)(p++),i>0?' ':'\n');
	  break;
      }
    }
  }
  return 0;
}

/*----------------------------------------------------------------*/

static void
usage(void)
{
  printf("usage: rdwrmem <parameters>\n");
  printf("  -i, --input  <name>                name of TWSI file [%s]\n",twsi_fname);
  printf("  -f  --filter <decimation>          TTSI data decimation\n");
  printf("  -m  --channel-mask <channel_mask>  TTSI channel mask\n");
  printf("  -c  --csv-mode                     select CSV output mode\n");
  printf("  -V, --version                      show version\n");
  printf("  -h  --help                         help\n");
}

int main(int argc, char *argv[])
{
  int res;
  int twsi_decimation = 50;
  int twsi_mask = 0x3f;
  int print_options = 0;

  sumint_twsi_raw_t twsiraw_struct;
  sumint_twsi_raw_t *twsiraw = &twsiraw_struct;

  static struct option long_opts[] = {
    { "input",        1, 0, 'i' },
    { "filter",       1, 0, 'f' },
    { "channel-mask", 1, 0, 'm' },
    { "csv-mode",     0, 0, 'c' },
    { "version",      0, 0, 'V' },
    { "help",         0, 0, 'h' },
    { 0,              0, 0, 0}
  };
  int opt;

  while ((opt = getopt_long(argc, argv, "i:f:m:cVh",
			    &long_opts[0], NULL)) != EOF)
  switch (opt) {
    case 'i':
      twsi_fname=optarg;
      break;
    case 'm':
      twsi_mask = strtoul(optarg,NULL,0);
      break;
    case 'f':
      twsi_decimation = strtoul(optarg,NULL,0);
      break;
    case 'c':
      print_options |= SUMINT_TWSI_PRTOPT_CSV;
      break;
    case 'V':
      fputs("SumInt TWSI capture analyze\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
  }

  res = sumint_twsi_raw_open(twsiraw, twsi_fname);
  if (res < 0) {
    fprintf(stderr, "TWSI file open error\n");
    exit(1);
  }

  res = sumint_twsi_raw_setup_frames(twsiraw, 1);
  if (res < 0) {
    fprintf(stderr, "TWSI frames setup error\n");
    exit(1);
  }

  if (twsi_decimation == 0) {
    res = sumint_twsi_raw_values_print(stdout, twsiraw, twsi_mask);
    if (res < 0) {
      fprintf(stderr, "TWSI print raw values %d\n", res);
      exit(1);
    }
  } else {
    res = sumint_twsi_filtered_values_print(stdout, twsiraw, twsi_mask, twsi_decimation, print_options);
    if (res < 0) {
      fprintf(stderr, "TWSI print filtered values %d\n", res);
      exit(1);
    }
  }

  return 0;
}
