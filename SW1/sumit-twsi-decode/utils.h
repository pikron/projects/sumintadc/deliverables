#ifndef _UTILS_DEF_H_
#define _UTILS_DEF_H_


/* skip space/blank characters, return 0 if no space found */
int si_skspace(char **ps);

/* skip spaces and checks for <sepchars>, */
/* if no such char return -1, else char is returned */
int si_fndsep(char **ps,char *sepchrs);

/* reads max <n> letters and digits to <pout> */
/* returns number of readed chars */
int si_alnumn(char **ps,char *pout,int n);

/* same as above, but only letters are read */
int si_alphan(char **ps,char *pout,int n);

/* reads long number, if no digit found return -1 */
int si_long(char **ps,long *val,int base);

/* reads unsigned long number, if no digit found return -1 */
int si_ulong(char **ps,long *val,int base);

/* reads numbers into array, size of element representation is selected by blen */
int si_add_to_arr(char **ps, void **pdata,int *plen, int base, int elsize, char *stop_chars);

/* concatenate C main style arguments into one line */
int concat_args2line(char **pline, int argc, char **argv);

#endif /* _UTILS_DEF_H_ */

