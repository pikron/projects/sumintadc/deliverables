/*******************************************************************
  Simple program to check LCD functionality on MicroZed
  based MZ_APO board designed by Petr Porazil at PiKRON

  mzapo_phys.h      - mapping of the physical address to process

  (C) Copyright 2017 by Pavel Pisa
      e-mail:   pisa@cmp.felk.cvut.cz
      homepage: http://cmp.felk.cvut.cz/~pisa
      company:  http://www.pikron.com/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#ifndef MZAPO_PHYS_H
#define MZAPO_PHYS_H

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct mmaped_file_region {
   void *data_start;
   size_t data_size;
   void *map_start;
   size_t map_size;
   off_t region_base;
} mmaped_file_region_t;

mmaped_file_region_t *mmap_file_region(int fd, off_t region_base,
                                       ssize_t region_size, int rdwr);

mmaped_file_region_t *mmap_file(const char *fname, int opt_sync, int rdwr);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif  /*MZAPO_PHYS_H*/
