/**
 * Dump data from Zlogan.
 * Requires udmabuf kernel driver (https://github.com/ikwzm/udmabuf),
 * expects a configured /dev/udmabuf0.
 *
 * Currently uses hard-coded peripheral addresses -> TODO: pull them from
 *   device tree and remake this as a kernel module.
 * Also check that DMA_LENGTH_BITS here is the same as set in DMA peripheral.
 * Zlogan version, number of signals and DMA word size are read from Zlogan IP
 * and are included in the dump header.
 *
 * Example:
 *   ./rxla -n $((16*1024*1024)) -o out.bin
 */
#define _DEFAULT_SOURCE
#define _POSIX_C_SOURCE

#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <inttypes.h>
#include <signal.h>
#include <pthread.h>
#include <sys/time.h>
#include <math.h>
#include <ctype.h>

#include <endian.h>
#include <err.h>
#include <stdbool.h>

#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "log.h"
#include "hw.h"
#include "utils.h"

#include "dma_setup.h"

#include "sumint-twsi.h"

//------------------------------------------------------------------------------

int set_rt_prio_self();
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))
//------------------------------------------------------------------------------

volatile sig_atomic_t g_quit = 0;
unsigned WORD_SIZE = 0; // detected at runtime

//------------------------------------------------------------------------------

static inline void twsi_capture_enable() {
    __mb();
    mm_ctrl[TWSI_CAPTURE_REG_CR] |= TWSI_CAPTURE_CR_EN;
    __mb();
}
//------------------------------------------------------------------------------

static inline void twsi_capture_disable(void) {
    __mb();
    mm_ctrl[TWSI_CAPTURE_REG_CR] &= ~TWSI_CAPTURE_CR_EN;
    __mb();
}
//------------------------------------------------------------------------------

static inline uint32_t twsi_capture_read_reg(uint32_t reg) {
    __mb();
    return mm_ctrl[reg];
}
//------------------------------------------------------------------------------

void dump_regs()
{
    char buf[512];
    char *p = buf;
    static const char * const names[] = {
        "CR", "SR", "LEN", "INP", "ID", "FIFO_DATA_COUNT",
        "FIFO_RD_DATA_COUNT", "FIFO_WR_DATA_COUNT", "SHADOW_LEN"
    };

    for (unsigned i=0; i<ARRAY_SIZE(names); ++i) {
        __mb();
        unsigned r = mm_ctrl[i];
        p += sprintf(p, "%s%s = 0x%08x (%u)", (i==0?"":", "), names[i], r, r);
    }
    log_wr(L_DEBUG, "%s", buf);
}

//------------------------------------------------------------------------------

int dma_save_data_to_file(const dma_setup_t *dma_setup, const void *data, size_t size)
{
    if (dma_setup->out != NULL)
        fwrite(data, 1, size, dma_setup->out);

    return 0;
}

//------------------------------------------------------------------------------

int twsi_decimation = 50;
int twsi_mask = 0x3f;

int dma_twsi_decode(const dma_setup_t *dma_setup, const void *data, size_t size)
{
    int res;

    void *copy = NULL;

    copy = malloc(size);
    if (copy == NULL)
      return -1;

    memcpy(copy, data, size);

    data = copy;

    sumint_twsi_raw_t twsiraw_struct;
    sumint_twsi_raw_t *twsiraw = &twsiraw_struct;

    res = sumint_twsi_in_memory(twsiraw, data, size);
    if (res < 0) {
        fprintf(stderr, "TWSI in memory map error\n");
        free(copy);
        return -1;
    }

    res = sumint_twsi_raw_setup_frames(twsiraw, 1);
    if (res < 0) {
        fprintf(stderr, "TWSI frames setup error\n");
        free(copy);
        return -1;
    }

    if (twsi_decimation) {
        res = sumint_twsi_filtered_values_print(dma_setup->out, twsiraw, twsi_mask, twsi_decimation, 0);
    } else {
        res = sumint_twsi_raw_values_print(dma_setup->out, twsiraw, twsi_mask);
    }
    fprintf(stderr, "TWSI decode returned %d\n", res);
    free(copy);
    return 0;
}

//------------------------------------------------------------------------------

dma_save_data_fnc_t dma_save_data_fnc = dma_save_data_to_file;

static void *dma_thread(void *arg)
{
    int res;
    const dma_setup_t *dma_setup = (const dma_setup_t *) arg;

    set_rt_prio_self();

    res = dma_do_transfer(dma_setup,
                    twsi_capture_enable, twsi_capture_disable,
                    mm_ctrl + TWSI_CAPTURE_REG_LEN,
                    mm_ctrl + TWSI_CAPTURE_REG_SHADOW_LEN, dma_save_data_fnc);

    if (res < 0)
      fprintf(stderr, "dma_do_transfer returned %d\n", res);

    return NULL;
}

//------------------------------------------------------------------------------

int set_prio(pthread_t th, int policy, int prio)
{
    struct sched_param rtp;
    memset(&rtp, 0, sizeof(rtp));
    rtp.sched_priority = prio;
    int rc = pthread_setschedparam(th, policy, &rtp);
    if (rc) {
        fprintf(stderr, "pthread_setschedparam() failed\n");
        return -1;
    } else {
        fprintf(stderr, "pthread_setschedparam() ok!\n");
        return 0;
    }
}

//------------------------------------------------------------------------------

int twsi_daemon_port = 5577;

int server_socket_setup(int server_port)
{
  struct sockaddr_in address;
  int server_socket;
  int yes=1;

  if ((server_socket = socket(AF_INET,SOCK_STREAM,0)) < 0) {
    fprintf(stderr, "twsi-capture-server: the socket created error\n");
    exit(1);
  }

  /* Make listening socket's port reusable - must appear before bind */
  if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes,sizeof(yes)) < 0) {
    fprintf(stderr, "twsi-capture-server: setsockopt failure\n");
    exit(1);
  }

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(server_port);
  if (bind(server_socket,(struct sockaddr *)&address,sizeof(address)) < 0) {
    fprintf(stderr, "twsi-capture-server: bind error\n");
    exit(1);
  }

  /*  Create a connection queue and initialize readfds to handle input from server_socket. */
  if(listen(server_socket, 5) < 0) {
    fprintf(stderr, "gatecental: error to listen on socket\n");
    exit(1);
  }

  return server_socket;
}

int twsi_daemon(dma_setup_t *dma_setup)
{
  int server_socket;
  int client_socket;
  char *line = NULL;
  size_t line_size = 0;
  ssize_t line_len;
  char *s;
  char str[128];
  int res;
  long val;

  signal(SIGPIPE, SIG_IGN);

  server_socket = server_socket_setup(twsi_daemon_port);
  if (server_socket < 0) {
    return -1;
  }

  while (!g_quit) {
    struct sockaddr_in address;
    socklen_t addrlen;

    addrlen = sizeof(struct sockaddr_in);
    client_socket = accept(server_socket, (struct sockaddr *)&address, &addrlen);
    if (client_socket > 0) {
      FILE *file;

      file = fdopen(client_socket, "r+");
      if (!file) {
        close(client_socket);
        continue;
      }

      line_len = getline(&line, &line_size, file);
      if (line_len <= 0) {
        close(client_socket);
        continue;
      }
      while (line_len && ((line[line_len - 1] == '\n') ||
                          (line[line_len - 1] == '\r'))) {
        line[line_len - 1] = 0;
        line_len--;
      }

      fprintf(stderr, "clien command: %s\n", line);

      s = line;

      si_skspace(&s);
      si_alnumn(&s, str, sizeof(str));

      if (strcmp(str, "capture") == 0) {
        while (*s) {
          si_skspace(&s);
          si_alnumn(&s, str, sizeof(str));
          if (strcmp(str, "channel_mask") == 0) {
            val = -1;
            res = si_fndsep(&s,"=");
            if (res >= 0)
              res = si_long(&s, &val, 0);
            if (res >= 0) {
              twsi_mask = val;
            } else {
              fprintf(file, "channel mask incorrect\n");
              goto client_close;
            }
          } else if (strcmp(str, "decimation") == 0) {
            val = -1;
            res = si_fndsep(&s,"=");
            if (res >= 0)
              res = si_long(&s, &val, 0);
            if ((res >= 0) && (val >= 0) && (val <= 255)) {
              twsi_decimation = val;
            } else {
              fprintf(file, "decimation incorrect\n");
              goto client_close;
            }
          } else {
            fprintf(file, "Unknown parameter \"%s\"\n", str);
            goto client_close;
          }
        }
      } else {
        fprintf(file, "Unknown commad \"%s\"\n", str);
        goto client_close;
      }

      fprintf(file, "SumInt ADC data mask = 0x%x decimation = %d\n",
               twsi_mask, twsi_decimation);

      //fprintf(file, "Echo\n");
      //fwrite(line, 1, line_size, file);

      //fprintf(file, "11\t12\t13\t14\t15\t16\t17\t18\n");
      //fprintf(file, "21\t22\t23\t24\t25\t26\t27\t28\n");

      if (1) {
        dma_setup->out = file;

        hw_reset();

        usleep(10);

        dma_thread(dma_setup);

      }
client_close:
      fclose(file);
    }
  }
  return 0;
}


//------------------------------------------------------------------------------

int set_rt_prio_self()
{
    /* set "realtime" priority to calling thread */
    return set_prio(pthread_self(), SCHED_FIFO,
                    sched_get_priority_max(SCHED_FIFO));
}
//------------------------------------------------------------------------------

void handle_exit(int sig)
{
    (void) sig;
    g_quit = 1;
}
//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
    int daemon_mode_fl = 0;
    int twsi_decode_fl = 0;
    int ret = 0;
    int opt;
    dma_setup_t dma_setup = {
        .transfer_size_w = 0x400000,
        .out = stdout,
    };

    log_stream_add(stderr);
    log_level(L_DEBUG);

    optind = opterr = 0;
    while ((opt = getopt(argc, argv, "dtf:m:n:o:hq")) != -1) {
        switch (opt) {
        case 'd':
            daemon_mode_fl = 1;
            break;
        case 't':
            twsi_decode_fl = 1;
            dma_save_data_fnc = dma_twsi_decode;
            break;
        case 'f':
            twsi_decimation = strtoul(optarg, NULL, 0);
            break;
        case 'm':
            twsi_mask = strtoul(optarg, NULL, 0);
            break;
        case 'n':
            dma_setup.transfer_size_w = strtoul(optarg, NULL, 0);
            break;
        case 'o':
            dma_setup.out = fopen(optarg, "w");
            break;
        case 'q':
            log_level(L_WARN);
            break;
        case 'h':
            fprintf(stderr,
"Zlogan data receiver\n"
"\n"
"Usage: %s [OPTIONS]\n"
"Options:\n"
"    -d                    Daemon mode\n"
"    -t                    TTSI decode\n"
"    -f decimation         TTSI data decimation\n"
"    -m channel_mask       TTSI channel mask\n"
"    -n transfer_length    Total length of bytes to transfer. Will be rounded\n"
"                          up to Zlogan word size and minimal DMA transfer size.\n"
"    -o filename           File to write the data into. Defaults to stdout.\n"
"    -q                    Be less verbose (output level set to L_WARNING).\n"
                   , argv[0]);
            return 0;
        case ':':
            fprintf(stderr, "command line option -%c requires an argument\n",
                    optopt);
            return -1;
            break;
        case '?':
            fprintf(stderr, "unrecognized command line option -%c\n",
                    optopt);
            return -1;
            break;
        default:
            break;
        }
    }

    hw_init();
    mm_ctrl[TWSI_CAPTURE_REG_CR] |= TWSI_CAPTURE_CR_LA_RST; __mb(); // zlogan reset
    mm_ctrl[TWSI_CAPTURE_REG_CR] &= ~TWSI_CAPTURE_CR_LA_RST; __mb();

    uint32_t id = mm_ctrl[TWSI_CAPTURE_REG_ID];
    WORD_SIZE = TWSI_CAPTURE_ID_GET_WS(id);
    if (TWSI_CAPTURE_ID_GET_VER(id) != 2) {
        log_wr(L_ERR, "unsupported TWSI-Capture IP version (%u)", TWSI_CAPTURE_ID_GET_VER(id));
        ret = 1;
        goto cleanup;
    }
    if (dma_setup.transfer_size_w == 0)
        dma_setup.transfer_size_w = DMA_MAX_LEN_B;
    dma_setup.transfer_size_w /= WORD_SIZE;

    if (DMA_SIZE < WORD_SIZE*8)
        err(1, "DMA buffer too small: at least %u bytes required, got %u", WORD_SIZE*8, DMA_SIZE);

    struct sigaction sa = {0};
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = &handle_exit;
    sa.sa_flags = 0;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL); // prevent crashes on SIGPIPE

    mlockall(MCL_CURRENT | MCL_FUTURE);

    if (daemon_mode_fl) {

       twsi_daemon(&dma_setup);

    } else {

       dma_thread(&dma_setup);

    }
cleanup:
    fclose(dma_setup.out);
    return ret;
}
