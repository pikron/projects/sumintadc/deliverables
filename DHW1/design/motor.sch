EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 16
Title "SumIntADC NX breadboard"
Date "2021-11-12"
Rev "1.0"
Comp "PiKRON"
Comment1 "Pavel Píša, Petr Porazil, Jakub Ladman"
Comment2 "Motor Control with SumInt ADC Current Sensing - Common Part"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 8200 2200 850  800 
U 6158E0F0
F0 "Sheet6158E0EF" 50
F1 "switch.sch" 50
F2 "OUT" O R 9050 2600 50 
F3 "PWM_A" I L 8200 2400 50 
F4 "PWM_B" I L 8200 2500 50 
F5 "ADC_SW" I L 8200 2600 50 
F6 "ADC_OUT" O L 8200 2700 50 
$EndSheet
$Comp
L Interface:AM26LV32xD U301
U 1 1 61672E28
P 3200 3950
F 0 "U301" H 3450 5000 50  0000 C CNN
F 1 "AM26C32D" H 3500 4900 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4200 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/am26c32.pdf" H 3200 3550 50  0001 C CNN
	1    3200 3950
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J301
U 1 1 6167D03F
P 6750 3900
F 0 "J301" V 6850 4200 50  0000 L CNN
F 1 "Conn_02x05_Odd_Even" H 6400 3450 50  0000 L CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 6750 3900 50  0001 C CNN
F 3 "~" H 6750 3900 50  0001 C CNN
	1    6750 3900
	0    -1   1    0   
$EndComp
Wire Wire Line
	3700 3850 3850 3850
Wire Wire Line
	6550 3650 6550 3700
Wire Wire Line
	6350 4300 6550 4300
Wire Wire Line
	6550 4300 6550 4200
Wire Wire Line
	6350 3600 6650 3600
Wire Wire Line
	6650 3600 6650 3700
Wire Wire Line
	6100 3450 6100 4350
Wire Wire Line
	6100 4350 6650 4350
Wire Wire Line
	6650 4350 6650 4200
Wire Wire Line
	6750 3350 6750 3700
Wire Wire Line
	6000 3150 6000 4400
Wire Wire Line
	6000 4400 6750 4400
Wire Wire Line
	6750 4400 6750 4200
Wire Wire Line
	7150 4450 7150 3600
Wire Wire Line
	7150 3600 6850 3600
Wire Wire Line
	6850 3600 6850 3700
Wire Wire Line
	3700 4250 4750 4250
Wire Wire Line
	5900 4250 5900 4500
Wire Wire Line
	5900 4500 6850 4500
Wire Wire Line
	6850 4500 6850 4200
Wire Wire Line
	6950 3700 6950 3650
Wire Wire Line
	6950 3650 7050 3650
Wire Wire Line
	7050 3650 7050 4750
Wire Wire Line
	3700 4550 4650 4550
Wire Wire Line
	6950 4550 6950 4200
$Comp
L Device:R_Pack04 RN302
U 1 1 616945DB
P 5600 2900
F 0 "RN302" H 5788 2946 50  0000 L CNN
F 1 "4x10k" H 5788 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x1206" V 5875 2900 50  0001 C CNN
F 3 "~" H 5600 2900 50  0001 C CNN
	1    5600 2900
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Pack04 RN301
U 1 1 61695BC7
P 4750 2900
F 0 "RN301" H 4938 2946 50  0000 L CNN
F 1 "4x3k3" H 4938 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x1206" V 5025 2900 50  0001 C CNN
F 3 "~" H 4750 2900 50  0001 C CNN
	1    4750 2900
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Pack04 RN303
U 1 1 61696C9B
P 5600 5050
F 0 "RN303" H 5788 5096 50  0000 L CNN
F 1 "4x10k" H 5788 5005 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x1206" V 5875 5050 50  0001 C CNN
F 3 "~" H 5600 5050 50  0001 C CNN
	1    5600 5050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5800 5250 5700 5250
Connection ~ 5700 5250
Wire Wire Line
	5700 5250 5600 5250
Connection ~ 5600 5250
Wire Wire Line
	5600 5250 5500 5250
Wire Wire Line
	5800 2700 5700 2700
Connection ~ 5700 2700
Wire Wire Line
	5700 2700 5600 2700
Connection ~ 5600 2700
Wire Wire Line
	5600 2700 5500 2700
Wire Wire Line
	5500 2700 5250 2700
Connection ~ 5500 2700
Connection ~ 4950 2700
Wire Wire Line
	4950 2700 4850 2700
Connection ~ 4850 2700
Wire Wire Line
	4850 2700 4750 2700
Connection ~ 4750 2700
Wire Wire Line
	4750 2700 4650 2700
Wire Wire Line
	4650 3100 4650 4550
Wire Wire Line
	4750 4250 4750 3100
Text HLabel 900  4350 0    50   Output ~ 0
IRC_A
Text HLabel 900  4450 0    50   Output ~ 0
MARK
Text HLabel 900  4250 0    50   Output ~ 0
IRC_B
Wire Wire Line
	1250 4350 900  4350
Wire Wire Line
	1100 4450 900  4450
Wire Wire Line
	1250 4250 900  4250
Text HLabel 7900 3350 0    50   Input ~ 0
PWM_2_A
Text HLabel 7900 3450 0    50   Input ~ 0
PWM_2_B
Text HLabel 7900 2400 0    50   Input ~ 0
PWM_1_A
Text HLabel 7900 2500 0    50   Input ~ 0
PWM_1_B
Text HLabel 7900 4300 0    50   Input ~ 0
PWM_3_A
Text HLabel 7900 4400 0    50   Input ~ 0
PWM_3_B
Text HLabel 7900 5250 0    50   Input ~ 0
PWM_4_A
Text HLabel 7900 5350 0    50   Input ~ 0
PWM_4_B
Wire Wire Line
	7900 2400 8200 2400
Wire Wire Line
	7900 2500 8200 2500
$Comp
L Device:C_Small C?
U 1 1 622221CF
P 3850 3950
AR Path="/622221CF" Ref="C?"  Part="1" 
AR Path="/614102B7/6158E0F0/622221CF" Ref="C?"  Part="1" 
AR Path="/614102B7/61665CD2/622221CF" Ref="C?"  Part="1" 
AR Path="/614102B7/616662E9/622221CF" Ref="C?"  Part="1" 
AR Path="/614102B7/616662F1/622221CF" Ref="C?"  Part="1" 
AR Path="/614102B7/622221CF" Ref="C301"  Part="1" 
F 0 "C301" H 3758 3996 50  0000 R CNN
F 1 "100n" H 3758 3905 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3850 3950 50  0001 C CNN
F 3 "~" H 3850 3950 50  0001 C CNN
	1    3850 3950
	-1   0    0    -1  
$EndComp
Text Notes 6700 3300 0    50   ~ 10
IRC
Wire Wire Line
	8200 2600 7900 2600
Wire Wire Line
	8200 2700 7900 2700
Text HLabel 7900 2600 0    50   Input ~ 0
ADC_1_SW
Text HLabel 7900 2700 0    50   Output ~ 0
ADC_1_OUT
Text HLabel 7900 3550 0    50   Input ~ 0
ADC_2_SW
Text HLabel 7900 3650 0    50   Output ~ 0
ADC_2_OUT
Text HLabel 7900 4500 0    50   Input ~ 0
ADC_3_SW
Text HLabel 7900 4600 0    50   Output ~ 0
ADC_3_OUT
Text HLabel 7900 5450 0    50   Input ~ 0
ADC_4_SW
Text HLabel 7900 5550 0    50   Output ~ 0
ADC_4_OUT
$Sheet
S 8200 3150 850  800 
U 61A702A7
F0 "sheet61A7029B" 50
F1 "switch.sch" 50
F2 "OUT" O R 9050 3550 50 
F3 "PWM_A" I L 8200 3350 50 
F4 "PWM_B" I L 8200 3450 50 
F5 "ADC_SW" I L 8200 3550 50 
F6 "ADC_OUT" O L 8200 3650 50 
$EndSheet
Wire Wire Line
	7900 3350 8200 3350
Wire Wire Line
	7900 3450 8200 3450
Wire Wire Line
	8200 3550 7900 3550
Wire Wire Line
	8200 3650 7900 3650
$Sheet
S 8200 4100 850  800 
U 61A71E43
F0 "sheet61A71E37" 50
F1 "switch.sch" 50
F2 "OUT" O R 9050 4500 50 
F3 "PWM_A" I L 8200 4300 50 
F4 "PWM_B" I L 8200 4400 50 
F5 "ADC_SW" I L 8200 4500 50 
F6 "ADC_OUT" O L 8200 4600 50 
$EndSheet
Wire Wire Line
	7900 4300 8200 4300
Wire Wire Line
	7900 4400 8200 4400
Wire Wire Line
	8200 4500 7900 4500
Wire Wire Line
	8200 4600 7900 4600
$Sheet
S 8200 5050 850  800 
U 61A73D1E
F0 "sheet61A73D12" 50
F1 "switch.sch" 50
F2 "OUT" O R 9050 5450 50 
F3 "PWM_A" I L 8200 5250 50 
F4 "PWM_B" I L 8200 5350 50 
F5 "ADC_SW" I L 8200 5450 50 
F6 "ADC_OUT" O L 8200 5550 50 
$EndSheet
Wire Wire Line
	7900 5250 8200 5250
Wire Wire Line
	7900 5350 8200 5350
Wire Wire Line
	8200 5450 7900 5450
Wire Wire Line
	8200 5550 7900 5550
$Comp
L Connector:Screw_Terminal_01x03 J302
U 1 1 61AB7F18
P 9850 2400
F 0 "J302" H 9930 2442 50  0000 L CNN
F 1 "Screw_Terminal_01x03" V 10100 1950 50  0000 L CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MC_1,5_3-G-5.08_1x03_P5.08mm_Horizontal" H 9850 2400 50  0001 C CNN
F 3 "~" H 9850 2400 50  0001 C CNN
	1    9850 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x04 J303
U 1 1 61AB8D64
P 9850 3950
F 0 "J303" H 9930 3942 50  0000 L CNN
F 1 "Screw_Terminal_01x04" V 10150 3500 50  0000 L CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MC_1,5_4-G-5.08_1x04_P5.08mm_Horizontal" H 9850 3950 50  0001 C CNN
F 3 "~" H 9850 3950 50  0001 C CNN
	1    9850 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 3850 9400 3850
Wire Wire Line
	9400 3850 9400 2600
Wire Wire Line
	9400 2600 9050 2600
Wire Wire Line
	9650 3950 9300 3950
Wire Wire Line
	9300 3950 9300 3550
Wire Wire Line
	9300 3550 9050 3550
Wire Wire Line
	9650 4050 9300 4050
Wire Wire Line
	9300 4050 9300 4500
Wire Wire Line
	9300 4500 9050 4500
Wire Wire Line
	9650 4150 9400 4150
Wire Wire Line
	9400 4150 9400 5450
Wire Wire Line
	9400 5450 9050 5450
Wire Wire Line
	3850 4050 3700 4050
Wire Wire Line
	3850 4050 3850 4950
Wire Wire Line
	3850 4950 3200 4950
Wire Wire Line
	3850 3850 3850 2850
Wire Wire Line
	3850 2850 3200 2850
Wire Wire Line
	3200 2850 3200 2950
$Comp
L Regulator_Linear:LM2937xS U?
U 1 1 61B1D503
P 8700 1250
AR Path="/61B1D503" Ref="U?"  Part="1" 
AR Path="/615D4F53/61B1D503" Ref="U?"  Part="1" 
AR Path="/614102B7/61B1D503" Ref="U302"  Part="1" 
F 0 "U302" H 8700 1492 50  0000 C CNN
F 1 "LM2940CS-5.0" H 8700 1401 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-3_TabPin2" H 8700 1475 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2937.pdf" H 8700 1200 50  0001 C CNN
	1    8700 1250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9000 1250 9150 1250
Wire Wire Line
	8400 1250 8300 1250
$Comp
L Device:C_Small C?
U 1 1 61B91068
P 8150 1350
AR Path="/61B91068" Ref="C?"  Part="1" 
AR Path="/622F77ED/61B91068" Ref="C?"  Part="1" 
AR Path="/614102B7/61B91068" Ref="C303"  Part="1" 
F 0 "C303" H 8350 1550 50  0000 C CNN
F 1 "10u/16" V 8200 1550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8150 1350 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/396/mlcc02_e-1307760.pdf" H 8150 1350 50  0001 C CNN
	1    8150 1350
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61B9106E
P 8300 1350
AR Path="/61B9106E" Ref="C?"  Part="1" 
AR Path="/622F77ED/61B9106E" Ref="C?"  Part="1" 
AR Path="/614102B7/61B9106E" Ref="C304"  Part="1" 
F 0 "C304" H 8200 1550 50  0000 C CNN
F 1 "10u/16" V 8350 1550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8300 1350 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/396/mlcc02_e-1307760.pdf" H 8300 1350 50  0001 C CNN
	1    8300 1350
	-1   0    0    1   
$EndComp
Connection ~ 8150 1250
Connection ~ 8300 1250
Wire Wire Line
	8300 1250 8150 1250
Wire Wire Line
	8150 1450 8150 1700
Wire Wire Line
	8150 1700 8300 1700
Connection ~ 8700 1700
Wire Wire Line
	8700 1700 8700 1550
Wire Wire Line
	8300 1450 8300 1700
Connection ~ 8300 1700
Wire Wire Line
	8300 1700 8700 1700
Connection ~ 3850 3850
Connection ~ 3850 4050
Wire Wire Line
	5100 6650 4950 6650
Wire Wire Line
	4600 6650 4750 6650
$Comp
L Device:R_Small R?
U 1 1 61C123B3
P 4850 6650
AR Path="/61C123B3" Ref="R?"  Part="1" 
AR Path="/614102B7/6158E0F0/61C123B3" Ref="R?"  Part="1" 
AR Path="/614102B7/61665CD2/61C123B3" Ref="R?"  Part="1" 
AR Path="/614102B7/616662E9/61C123B3" Ref="R?"  Part="1" 
AR Path="/614102B7/616662F1/61C123B3" Ref="R?"  Part="1" 
AR Path="/614102B7/61A702A7/61C123B3" Ref="R?"  Part="1" 
AR Path="/614102B7/61A71E43/61C123B3" Ref="R?"  Part="1" 
AR Path="/614102B7/61A73D1E/61C123B3" Ref="R?"  Part="1" 
AR Path="/614102B7/61C123B3" Ref="R301"  Part="1" 
F 0 "R301" V 4750 6650 50  0000 C CNN
F 1 "4M7" V 4950 6650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4850 6650 50  0001 C CNN
F 3 "~" H 4850 6650 50  0001 C CNN
	1    4850 6650
	0    -1   1    0   
$EndComp
$Comp
L power:+24V #PWR0311
U 1 1 6180BDB7
P 9150 1250
F 0 "#PWR0311" H 9150 1100 50  0001 C CNN
F 1 "+24V" H 9165 1423 50  0000 C CNN
F 2 "" H 9150 1250 50  0001 C CNN
F 3 "" H 9150 1250 50  0001 C CNN
	1    9150 1250
	1    0    0    -1  
$EndComp
$Comp
L project_power:GNDM #PWR0313
U 1 1 6180D20C
P 9650 2500
F 0 "#PWR0313" H 9650 2250 50  0001 C CNN
F 1 "GNDM" H 9655 2327 50  0000 C CNN
F 2 "" H 9650 2500 50  0001 C CNN
F 3 "" H 9650 2500 50  0001 C CNN
	1    9650 2500
	1    0    0    -1  
$EndComp
$Comp
L project_power:GNDM #PWR0305
U 1 1 6181A0EB
P 5800 5250
F 0 "#PWR0305" H 5800 5000 50  0001 C CNN
F 1 "GNDM" H 5805 5077 50  0000 C CNN
F 2 "" H 5800 5250 50  0001 C CNN
F 3 "" H 5800 5250 50  0001 C CNN
	1    5800 5250
	1    0    0    -1  
$EndComp
Connection ~ 5800 5250
NoConn ~ 9650 2400
$Comp
L power:GND #PWR0304
U 1 1 61820EE0
P 4600 6650
F 0 "#PWR0304" H 4600 6400 50  0001 C CNN
F 1 "GND" H 4605 6477 50  0000 C CNN
F 2 "" H 4600 6650 50  0001 C CNN
F 3 "" H 4600 6650 50  0001 C CNN
	1    4600 6650
	1    0    0    -1  
$EndComp
$Comp
L project_power:GNDM #PWR0306
U 1 1 618217AC
P 5100 6650
F 0 "#PWR0306" H 5100 6400 50  0001 C CNN
F 1 "GNDM" H 5105 6477 50  0000 C CNN
F 2 "" H 5100 6650 50  0001 C CNN
F 3 "" H 5100 6650 50  0001 C CNN
	1    5100 6650
	1    0    0    -1  
$EndComp
$Comp
L project_power:+5VM #PWR0309
U 1 1 6182868B
P 8150 1250
F 0 "#PWR0309" H 8150 1100 50  0001 C CNN
F 1 "+5VM" H 8165 1423 50  0000 C CNN
F 2 "" H 8150 1250 50  0001 C CNN
F 3 "" H 8150 1250 50  0001 C CNN
	1    8150 1250
	1    0    0    -1  
$EndComp
$Comp
L project_power:+5VM #PWR0303
U 1 1 61828DF8
P 5250 2700
F 0 "#PWR0303" H 5250 2550 50  0001 C CNN
F 1 "+5VM" H 5265 2873 50  0000 C CNN
F 2 "" H 5250 2700 50  0001 C CNN
F 3 "" H 5250 2700 50  0001 C CNN
	1    5250 2700
	1    0    0    -1  
$EndComp
$Comp
L project_power:GNDM #PWR0310
U 1 1 61829745
P 8700 1700
F 0 "#PWR0310" H 8700 1450 50  0001 C CNN
F 1 "GNDM" H 8705 1527 50  0000 C CNN
F 2 "" H 8700 1700 50  0001 C CNN
F 3 "" H 8700 1700 50  0001 C CNN
	1    8700 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0312
U 1 1 61829B03
P 9650 2300
F 0 "#PWR0312" H 9650 2150 50  0001 C CNN
F 1 "+24V" H 9665 2473 50  0000 C CNN
F 2 "" H 9650 2300 50  0001 C CNN
F 3 "" H 9650 2300 50  0001 C CNN
	1    9650 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61BB1622
P 9150 1350
AR Path="/61BB1622" Ref="C?"  Part="1" 
AR Path="/614102B7/6158E0F0/61BB1622" Ref="C?"  Part="1" 
AR Path="/614102B7/61665CD2/61BB1622" Ref="C?"  Part="1" 
AR Path="/614102B7/616662E9/61BB1622" Ref="C?"  Part="1" 
AR Path="/614102B7/616662F1/61BB1622" Ref="C?"  Part="1" 
AR Path="/614102B7/61BB1622" Ref="C305"  Part="1" 
F 0 "C305" H 9058 1396 50  0000 R CNN
F 1 "100n" H 9058 1305 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9150 1350 50  0001 C CNN
F 3 "~" H 9150 1350 50  0001 C CNN
	1    9150 1350
	-1   0    0    -1  
$EndComp
Connection ~ 9150 1250
Wire Wire Line
	9150 1450 9150 1700
Wire Wire Line
	8700 1700 9150 1700
$Comp
L Device:C_Small C?
U 1 1 6185299A
P 4850 6400
AR Path="/6185299A" Ref="C?"  Part="1" 
AR Path="/614102B7/6158E0F0/6185299A" Ref="C?"  Part="1" 
AR Path="/614102B7/61665CD2/6185299A" Ref="C?"  Part="1" 
AR Path="/614102B7/616662E9/6185299A" Ref="C?"  Part="1" 
AR Path="/614102B7/616662F1/6185299A" Ref="C?"  Part="1" 
AR Path="/614102B7/6185299A" Ref="C302"  Part="1" 
F 0 "C302" V 5079 6400 50  0000 C CNN
F 1 "100n" V 4988 6400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4850 6400 50  0001 C CNN
F 3 "~" H 4850 6400 50  0001 C CNN
	1    4850 6400
	0    1    -1   0   
$EndComp
Wire Wire Line
	4950 6400 5100 6400
Wire Wire Line
	5100 6400 5100 6650
Connection ~ 5100 6650
Wire Wire Line
	4750 6400 4600 6400
Wire Wire Line
	4600 6400 4600 6650
Connection ~ 4600 6650
$Comp
L project_power:+5VM #PWR0308
U 1 1 61863FAA
P 6350 4300
F 0 "#PWR0308" H 6350 4150 50  0001 C CNN
F 1 "+5VM" H 6365 4473 50  0000 C CNN
F 2 "" H 6350 4300 50  0001 C CNN
F 3 "" H 6350 4300 50  0001 C CNN
	1    6350 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3600 6350 3700
$Comp
L project_power:GNDM #PWR0307
U 1 1 6186F308
P 6350 3700
F 0 "#PWR0307" H 6350 3450 50  0001 C CNN
F 1 "GNDM" H 6355 3527 50  0000 C CNN
F 2 "" H 6350 3700 50  0001 C CNN
F 3 "" H 6350 3700 50  0001 C CNN
	1    6350 3700
	1    0    0    -1  
$EndComp
Connection ~ 5250 2700
Wire Wire Line
	5250 2700 4950 2700
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 62431341
P 9650 2300
F 0 "#FLG0105" H 9650 2375 50  0001 C CNN
F 1 "PWR_FLAG" V 9650 2427 50  0000 L CNN
F 2 "" H 9650 2300 50  0001 C CNN
F 3 "~" H 9650 2300 50  0001 C CNN
	1    9650 2300
	0    -1   -1   0   
$EndComp
Connection ~ 9650 2300
$Comp
L power:PWR_FLAG #FLG0106
U 1 1 62431B4F
P 9650 2500
F 0 "#FLG0106" H 9650 2575 50  0001 C CNN
F 1 "PWR_FLAG" V 9650 2627 50  0000 L CNN
F 2 "" H 9650 2500 50  0001 C CNN
F 3 "~" H 9650 2500 50  0001 C CNN
	1    9650 2500
	0    -1   -1   0   
$EndComp
Connection ~ 9650 2500
Wire Wire Line
	3700 3150 4850 3150
Wire Wire Line
	4950 3100 4950 3450
Connection ~ 4950 3450
Wire Wire Line
	4950 3450 6100 3450
Wire Wire Line
	3700 3450 4950 3450
Connection ~ 4650 4550
Wire Wire Line
	4650 4550 6950 4550
Connection ~ 4750 4250
Wire Wire Line
	4750 4250 5900 4250
Wire Wire Line
	4850 3100 4850 3150
Connection ~ 4850 3150
Wire Wire Line
	4850 3150 6000 3150
Wire Wire Line
	3700 3650 5800 3650
Wire Wire Line
	5500 3100 5500 4750
Wire Wire Line
	3700 4750 5500 4750
Wire Wire Line
	5600 3100 5600 4450
Wire Wire Line
	3700 4450 5600 4450
Wire Wire Line
	5700 3100 5700 3350
Wire Wire Line
	3700 3350 5700 3350
Wire Wire Line
	5800 3100 5800 3650
Connection ~ 5800 3650
Wire Wire Line
	5800 3650 6550 3650
Wire Wire Line
	5800 3650 5800 4850
Connection ~ 5700 3350
Wire Wire Line
	5700 3350 5700 4850
Wire Wire Line
	5700 3350 6750 3350
Connection ~ 5600 4450
Wire Wire Line
	5600 4450 5600 4850
Wire Wire Line
	5600 4450 7150 4450
Connection ~ 5500 4750
Wire Wire Line
	5500 4750 5500 4850
Wire Wire Line
	5500 4750 7050 4750
$Comp
L Connector:TestPoint TP?
U 1 1 61A7E0AF
P 5500 6650
AR Path="/61A7E0AF" Ref="TP?"  Part="1" 
AR Path="/614102B7/61A7E0AF" Ref="TP301"  Part="1" 
F 0 "TP301" H 5500 6975 50  0000 C CNN
F 1 "TestPoint" H 5500 6884 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 5700 6650 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/215/000-5004-741320.pdf" H 5700 6650 50  0001 C CNN
	1    5500 6650
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61A7E0BC
P 5850 6650
AR Path="/61A7E0BC" Ref="TP?"  Part="1" 
AR Path="/614102B7/61A7E0BC" Ref="TP302"  Part="1" 
F 0 "TP302" H 5850 6975 50  0000 C CNN
F 1 "TestPoint" H 5850 6884 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 6050 6650 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/215/000-5004-741320.pdf" H 6050 6650 50  0001 C CNN
	1    5850 6650
	1    0    0    -1  
$EndComp
Connection ~ 5850 6650
Wire Wire Line
	5850 6650 6200 6650
Wire Wire Line
	5500 6650 5850 6650
$Comp
L Connector:TestPoint TP?
U 1 1 61A7E0C5
P 6200 6650
AR Path="/61A7E0C5" Ref="TP?"  Part="1" 
AR Path="/614102B7/61A7E0C5" Ref="TP303"  Part="1" 
F 0 "TP303" H 6200 6975 50  0000 C CNN
F 1 "TestPoint" H 6200 6884 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 6400 6650 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/215/000-5004-741320.pdf" H 6400 6650 50  0001 C CNN
	1    6200 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 6650 6550 6650
$Comp
L Connector:TestPoint TP?
U 1 1 61A7E0CC
P 6550 6650
AR Path="/61A7E0CC" Ref="TP?"  Part="1" 
AR Path="/614102B7/61A7E0CC" Ref="TP304"  Part="1" 
F 0 "TP304" H 6550 6975 50  0000 C CNN
F 1 "TestPoint" H 6550 6884 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 6750 6650 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/215/000-5004-741320.pdf" H 6750 6650 50  0001 C CNN
	1    6550 6650
	1    0    0    -1  
$EndComp
Connection ~ 6200 6650
$Comp
L project_power:GNDM #PWR0314
U 1 1 61A887C3
P 5500 6650
F 0 "#PWR0314" H 5500 6400 50  0001 C CNN
F 1 "GNDM" H 5505 6477 50  0000 C CNN
F 2 "" H 5500 6650 50  0001 C CNN
F 3 "" H 5500 6650 50  0001 C CNN
	1    5500 6650
	1    0    0    -1  
$EndComp
Connection ~ 5500 6650
$Comp
L project_power:+5VM #PWR0318
U 1 1 6190C6C8
P 3200 2850
F 0 "#PWR0318" H 3200 2700 50  0001 C CNN
F 1 "+5VM" H 3215 3023 50  0000 C CNN
F 2 "" H 3200 2850 50  0001 C CNN
F 3 "" H 3200 2850 50  0001 C CNN
	1    3200 2850
	1    0    0    -1  
$EndComp
Connection ~ 3200 2850
$Comp
L project_power:GNDM #PWR0319
U 1 1 6190CD23
P 3200 4950
F 0 "#PWR0319" H 3200 4700 50  0001 C CNN
F 1 "GNDM" H 3205 4777 50  0000 C CNN
F 2 "" H 3200 4950 50  0001 C CNN
F 3 "" H 3200 4950 50  0001 C CNN
	1    3200 4950
	1    0    0    -1  
$EndComp
Connection ~ 3200 4950
$Comp
L Isolator:ISO7341C U?
U 1 1 6194508C
P 1650 4050
AR Path="/6194508C" Ref="U?"  Part="1" 
AR Path="/61A3FEC3/6194508C" Ref="U?"  Part="1" 
AR Path="/622F77ED/6194508C" Ref="U?"  Part="1" 
AR Path="/614102B7/6158E0F0/6194508C" Ref="U?"  Part="1" 
AR Path="/614102B7/61665CD2/6194508C" Ref="U?"  Part="1" 
AR Path="/614102B7/616662E9/6194508C" Ref="U?"  Part="1" 
AR Path="/614102B7/616662F1/6194508C" Ref="U?"  Part="1" 
AR Path="/614102B7/61A702A7/6194508C" Ref="U?"  Part="1" 
AR Path="/614102B7/61A71E43/6194508C" Ref="U?"  Part="1" 
AR Path="/614102B7/61A73D1E/6194508C" Ref="U?"  Part="1" 
AR Path="/614102B7/6194508C" Ref="U303"  Part="1" 
F 0 "U303" H 1650 4717 50  0000 C CNN
F 1 "ISOS141-SEP" H 1650 4626 50  0000 C CNN
F 2 "Package_SO:SSOP-16_3.9x4.9mm_P0.635mm" H 1650 3500 50  0001 C CIN
F 3 "https://www.ti.com/lit/gpn/isos141-sep" H 1650 4450 50  0001 C CNN
	1    1650 4050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2050 3950 2100 3950
Wire Wire Line
	2050 3650 2100 3650
Connection ~ 2100 3650
Wire Wire Line
	2100 3650 2100 3950
Wire Wire Line
	2050 3750 2300 3750
$Comp
L Device:C_Small C?
U 1 1 61945097
P 2200 3650
AR Path="/61945097" Ref="C?"  Part="1" 
AR Path="/614102B7/6158E0F0/61945097" Ref="C?"  Part="1" 
AR Path="/614102B7/61665CD2/61945097" Ref="C?"  Part="1" 
AR Path="/614102B7/616662E9/61945097" Ref="C?"  Part="1" 
AR Path="/614102B7/616662F1/61945097" Ref="C?"  Part="1" 
AR Path="/614102B7/61A702A7/61945097" Ref="C?"  Part="1" 
AR Path="/614102B7/61A71E43/61945097" Ref="C?"  Part="1" 
AR Path="/614102B7/61A73D1E/61945097" Ref="C?"  Part="1" 
AR Path="/614102B7/61945097" Ref="C308"  Part="1" 
F 0 "C308" V 2000 3500 50  0000 C CNN
F 1 "100n" V 2100 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2200 3650 50  0001 C CNN
F 3 "~" H 2200 3650 50  0001 C CNN
	1    2200 3650
	0    -1   1    0   
$EndComp
Wire Wire Line
	2300 3650 2300 3750
Connection ~ 2300 3750
$Comp
L project_power:GNDM #PWR?
U 1 1 6194509F
P 2300 3750
AR Path="/614102B7/6158E0F0/6194509F" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A73D1E/6194509F" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A702A7/6194509F" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A71E43/6194509F" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/6194509F" Ref="#PWR0317"  Part="1" 
F 0 "#PWR0317" H 2300 3500 50  0001 C CNN
F 1 "GNDM" H 2305 3577 50  0000 C CNN
F 2 "" H 2300 3750 50  0001 C CNN
F 3 "" H 2300 3750 50  0001 C CNN
	1    2300 3750
	1    0    0    -1  
$EndComp
$Comp
L project_power:+5VM #PWR?
U 1 1 619450A5
P 2100 3650
AR Path="/614102B7/6158E0F0/619450A5" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A73D1E/619450A5" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A702A7/619450A5" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A71E43/619450A5" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/619450A5" Ref="#PWR0316"  Part="1" 
F 0 "#PWR0316" H 2100 3500 50  0001 C CNN
F 1 "+5VM" H 2115 3823 50  0000 C CNN
F 2 "" H 2100 3650 50  0001 C CNN
F 3 "" H 2100 3650 50  0001 C CNN
	1    2100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 3650 1200 3650
Wire Wire Line
	1250 3950 1200 3950
Wire Wire Line
	1200 3950 1200 3650
$Comp
L Device:C_Small C?
U 1 1 619450AE
P 1100 3650
AR Path="/619450AE" Ref="C?"  Part="1" 
AR Path="/614102B7/6158E0F0/619450AE" Ref="C?"  Part="1" 
AR Path="/614102B7/61665CD2/619450AE" Ref="C?"  Part="1" 
AR Path="/614102B7/616662E9/619450AE" Ref="C?"  Part="1" 
AR Path="/614102B7/616662F1/619450AE" Ref="C?"  Part="1" 
AR Path="/614102B7/61A702A7/619450AE" Ref="C?"  Part="1" 
AR Path="/614102B7/61A71E43/619450AE" Ref="C?"  Part="1" 
AR Path="/614102B7/61A73D1E/619450AE" Ref="C?"  Part="1" 
AR Path="/614102B7/619450AE" Ref="C307"  Part="1" 
F 0 "C307" V 1350 3550 50  0000 C CNN
F 1 "100n" V 1250 3550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1100 3650 50  0001 C CNN
F 3 "~" H 1100 3650 50  0001 C CNN
	1    1100 3650
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 619450B4
P 1000 3750
AR Path="/614102B7/6158E0F0/619450B4" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A73D1E/619450B4" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A702A7/619450B4" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A71E43/619450B4" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/619450B4" Ref="#PWR0301"  Part="1" 
F 0 "#PWR0301" H 1000 3500 50  0001 C CNN
F 1 "GND" H 1005 3577 50  0000 C CNN
F 2 "" H 1000 3750 50  0001 C CNN
F 3 "" H 1000 3750 50  0001 C CNN
	1    1000 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+2V5 #PWR?
U 1 1 619450BA
P 1200 3650
AR Path="/614102B7/6158E0F0/619450BA" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A73D1E/619450BA" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A702A7/619450BA" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A71E43/619450BA" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/619450BA" Ref="#PWR0315"  Part="1" 
F 0 "#PWR0315" H 1200 3500 50  0001 C CNN
F 1 "+2V5" H 1215 3823 50  0000 C CNN
F 2 "" H 1200 3650 50  0001 C CNN
F 3 "" H 1200 3650 50  0001 C CNN
	1    1200 3650
	1    0    0    -1  
$EndComp
Connection ~ 1200 3650
Wire Wire Line
	1000 3750 1250 3750
Wire Wire Line
	1000 3650 1000 3750
Connection ~ 1000 3750
$Comp
L Device:R_Small R?
U 1 1 619450C4
P 2250 4450
AR Path="/619450C4" Ref="R?"  Part="1" 
AR Path="/614102B7/6158E0F0/619450C4" Ref="R?"  Part="1" 
AR Path="/614102B7/61665CD2/619450C4" Ref="R?"  Part="1" 
AR Path="/614102B7/616662E9/619450C4" Ref="R?"  Part="1" 
AR Path="/614102B7/616662F1/619450C4" Ref="R?"  Part="1" 
AR Path="/614102B7/61A702A7/619450C4" Ref="R?"  Part="1" 
AR Path="/614102B7/61A71E43/619450C4" Ref="R?"  Part="1" 
AR Path="/614102B7/61A73D1E/619450C4" Ref="R?"  Part="1" 
AR Path="/614102B7/619450C4" Ref="R303"  Part="1" 
F 0 "R303" V 2354 4450 50  0000 C CNN
F 1 "DNP" V 2445 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2250 4450 50  0001 C CNN
F 3 "~" H 2250 4450 50  0001 C CNN
	1    2250 4450
	0    -1   1    0   
$EndComp
Wire Wire Line
	2150 4450 2050 4450
$Comp
L Device:R_Small R?
U 1 1 619450CB
P 1100 4650
AR Path="/619450CB" Ref="R?"  Part="1" 
AR Path="/614102B7/6158E0F0/619450CB" Ref="R?"  Part="1" 
AR Path="/614102B7/61665CD2/619450CB" Ref="R?"  Part="1" 
AR Path="/614102B7/616662E9/619450CB" Ref="R?"  Part="1" 
AR Path="/614102B7/616662F1/619450CB" Ref="R?"  Part="1" 
AR Path="/614102B7/61A702A7/619450CB" Ref="R?"  Part="1" 
AR Path="/614102B7/61A71E43/619450CB" Ref="R?"  Part="1" 
AR Path="/614102B7/61A73D1E/619450CB" Ref="R?"  Part="1" 
AR Path="/614102B7/619450CB" Ref="R302"  Part="1" 
F 0 "R302" H 1150 4800 50  0000 L CNN
F 1 "10k" V 1200 4550 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 1100 4650 50  0001 C CNN
F 3 "~" H 1100 4650 50  0001 C CNN
	1    1100 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1250 4450 1100 4450
Wire Wire Line
	1100 4450 1100 4550
$Comp
L power:GND #PWR?
U 1 1 619450D3
P 1100 4750
AR Path="/614102B7/6158E0F0/619450D3" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A73D1E/619450D3" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A702A7/619450D3" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/61A71E43/619450D3" Ref="#PWR?"  Part="1" 
AR Path="/614102B7/619450D3" Ref="#PWR0302"  Part="1" 
F 0 "#PWR0302" H 1100 4500 50  0001 C CNN
F 1 "GND" H 1105 4577 50  0000 C CNN
F 2 "" H 1100 4750 50  0001 C CNN
F 3 "" H 1100 4750 50  0001 C CNN
	1    1100 4750
	1    0    0    -1  
$EndComp
Text Notes 1250 4850 0    50   Italic 0
ISO7340C\nor similar to enable\nMARK function
Connection ~ 1100 4450
Text HLabel 900  4150 0    50   Output ~ 0
IRC_I
Wire Wire Line
	1250 4150 900  4150
Wire Wire Line
	2700 3550 2550 3550
Wire Wire Line
	2550 3550 2550 4450
Wire Wire Line
	2550 4450 2350 4450
Wire Wire Line
	2050 4350 2450 4350
Wire Wire Line
	2450 4350 2450 4650
Wire Wire Line
	2450 4650 2700 4650
Wire Wire Line
	2050 4250 2650 4250
Wire Wire Line
	2650 4250 2650 4350
Wire Wire Line
	2650 4350 2700 4350
Wire Wire Line
	2050 4150 2450 4150
Wire Wire Line
	2450 4150 2450 3250
Wire Wire Line
	2450 3250 2700 3250
$Comp
L Connector:TestPoint TP?
U 1 1 6195FA72
P 8150 1250
AR Path="/6195FA72" Ref="TP?"  Part="1" 
AR Path="/614102B7/6195FA72" Ref="TP305"  Part="1" 
F 0 "TP305" V 8196 1437 50  0000 L CNN
F 1 "TestPoint" V 8105 1437 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 8350 1250 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/215/000-5004-741320.pdf" H 8350 1250 50  0001 C CNN
	1    8150 1250
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
