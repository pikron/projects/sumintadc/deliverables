% Design check
% Single supply version

% componenets values
R1=100e3;
R2=33e3;

R3=100e3;
R4=33e3;
R5=15e3;

C1=470e-12;

% Cycle period
Tmod=5e-6; %200kHz

% Input voltage range
Vinmax=1;
Vinmin=-1;

% Refference voltage
Vref=2.5;

V0=Vref*R2/(R1+R2);

IC1_P0=(Vref-V0)/R3+(Vref-V0)/R5-V0/R4;
IC1_N0=(Vref-V0)/R3-V0/R5-V0/R4;

t1=Tmod*-IC1_N0/(IC1_P0-IC1_N0);

% Optimal duty cycle range is 0.05 to 0.45
% Optimal output voltage swing depend on opamp supply,
%   opamp slew rate and comparator noise

% Duty cycle for zerro input voltage
D_0=t1/Tmod
% Output voltage swing for zerro input voltage
Uo_0=t1*IC1_P0/C1

IC1_Pmax=(Vref-V0)/R3+(Vref-V0)/R5-(V0-Vinmax)/R4;
IC1_Nmax=(Vref-V0)/R3-V0/R5-(V0-Vinmax)/R4;

t1max=Tmod*-IC1_Nmax/(IC1_Pmax-IC1_Nmax);
% Duty cycle for maximal input voltage
D_max=t1max/Tmod
% Output voltage swing for maximal input voltage
Uo_max=t1max*IC1_Pmax/C1

IC1_Pmin=(Vref-V0)/R3+(Vref-V0)/R5-(V0-Vinmin)/R4;
IC1_Nmin=(Vref-V0)/R3-V0/R5-(V0-Vinmin)/R4;

t1min=Tmod*-IC1_Nmin/(IC1_Pmin-IC1_Nmin);
% Duty cycle for maximal input voltage
D_min=t1min/Tmod
% Output voltage swing for minimal input voltage
Uo_min=t1min*IC1_Pmin/C1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Symmetrical supply version

% Componenets values 
R6=47e3;
R7=47e3/2;

C1=1.8e-9;

% Cycle period
Tmod=100e-6; %10kHz

% Input voltage range
Vinmax=1;
Vinmin=-1;

% Positive refference voltage
Vrp=2.5;

% Negative refference voltage
Vrn=-Vrp/3;


IC1_P0=Vrp/R7;
IC1_N0=Vrn/R7;

t1=Tmod*-IC1_N0/(IC1_P0-IC1_N0);

% Duty cycle for zerro input voltage
D_0=t1/Tmod
% Output voltage swing for zerro input voltage
Vo_0=t1*IC1_P0/C1

IC1_Pmax=Vrp/R7+Vinmax/R6;
IC1_Nmax=Vrn/R7+Vinmax/R6;

t1max=Tmod*-IC1_Nmax/(IC1_Pmax-IC1_Nmax);
% Duty cycle for maximal input voltage
D_max=t1max/Tmod
% Output voltage swing for maximal input voltage
Vo_max=t1max*IC1_Pmax/C1

IC1_Pmin=Vrp/R7+Vinmin/R6;
IC1_Nmin=Vrn/R7+Vinmin/R6;

t1min=Tmod*-IC1_Nmin/(IC1_Pmin-IC1_Nmin);
% Duty cycle for minimal input voltage
D_min=t1min/Tmod
% Output voltage swing for maximal input voltage
Vo_min=t1min*IC1_Pmin/C1
